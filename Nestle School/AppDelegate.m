//
//  AppDelegate.m
//  Nestle School
//
//  Created by MOREMO on 9/21/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "AppDelegate.h"

#import "LoginViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate ()
@end

@implementation AppDelegate

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSDKAppEvents activateApp];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"registered"])
    {
        UITabBarController *tabcontroller = [self.window.rootViewController.storyboard
                                          instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        tabcontroller.selectedIndex = 2;
        [tabcontroller.tabBar setBackgroundColor:[UIColor whiteColor]];
        
        UITabBar *tabBar = tabcontroller.tabBar;
        UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
        UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
        UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
        UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
        UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
        
        tabBarItem1.selectedImage = [[UIImage imageNamed:@"ico_1_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabBarItem1.image = [[UIImage imageNamed:@"ico_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        tabBarItem2.selectedImage = [[UIImage imageNamed:@"ico_2_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabBarItem2.image = [[UIImage imageNamed:@"ico_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        tabBarItem3.selectedImage = [[UIImage imageNamed:@"BUTTON3-2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabBarItem3.image = [[UIImage imageNamed:@"BUTTON3.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        tabBarItem4.selectedImage = [[UIImage imageNamed:@"ico_3_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabBarItem4.image = [[UIImage imageNamed:@"ico_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        tabBarItem5.selectedImage = [[UIImage imageNamed:@"ico_4_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabBarItem5.image = [[UIImage imageNamed:@"ico_4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        self.window.rootViewController = tabcontroller;
    }
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.pageIndicatorTintColor = [UIColor grayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
    
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

//- (void)applicationDidBecomeActive:(UIApplication *)application {
//    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
