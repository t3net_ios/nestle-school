//
//  ClassClipViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/29/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassLessonViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
{
    NSDictionary *dictionary;
    NSArray *groupArray;
    NSArray *bannerArray;
    NSArray *imageBanner;
    
    NSMutableSet* _collapsedSections;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *classLessonlbl;

@property (nonatomic, strong) NSString *cellName;
@property (nonatomic, strong) NSString *cellTid;

@property (nonatomic, strong) NSString *tidClassLesson;


- (IBAction)sortby:(id)sender;

- (IBAction)back:(id)sender;

@end

//for (NSDictionary *dataDict in bannerArray)
//{
//    NSString *dic_nid = [dataDict objectForKey:@"tid"];
//    NSString *dic_title = [dataDict objectForKey:@"title"];
//    NSString *dic_field_images = [dataDict objectForKey:@"field_banner_image"];
//    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        NSURL  *url = [NSURL URLWithString:dic_field_images];
//        NSData *urlData = [NSData dataWithContentsOfURL:url];
//        
//        if ( urlData )
//        {
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsDirectory = [paths objectAtIndex:0];
//            
//            NSString  *filePath = [NSString stringWithFormat:@"%@/%@/%@.png", documentsDirectory,dic_nid,dic_title];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [urlData writeToFile:filePath atomically:YES];
//                //NSLog(@"File Saved !");
//                bannerImage.image = [UIImage imageWithData:urlData];
//            });
//        }
//    });
//}
