//
//  ClassClipViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/29/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ClassLessonViewController.h"
#import "RESTAPI.h"

#import "ClassSubLessonViewController.h"
#import "SCLAlertView.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface ClassLessonViewController ()
@end

@implementation ClassLessonViewController

@synthesize classLessonlbl;

@synthesize cellName, cellTid;
@synthesize tidClassLesson;
@synthesize scrollView, pageControl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    classLessonlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    classLessonlbl.layer.shadowOffset = CGSizeMake(0, 1);
    classLessonlbl.layer.shadowOpacity = 1.0;
    classLessonlbl.layer.shadowRadius = 1.0;
    
    scrollView.delegate = self;
    
    //////////////////////////////////////////////////
    classLessonlbl.text = cellName;
    
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = Color;
    
    [self getGroup];
    [self getBanner];
}

-(void)getGroup
{
//    NSString *urlString = [NSString stringWithFormat:@"service/group?subject_id=%@", cellTid];
//    NSLog(@"%@",urlString);
    
    NSDictionary *profileData = [[NSUserDefaults standardUserDefaults] valueForKey:@"profile"];
    
    NSString *urlString = [NSString stringWithFormat:@"service/group_watchlater/%@/%@", cellTid, [profileData valueForKey:@"uid"]];
    NSLog(@"%@",urlString);
    
    groupArray = [RESTAPI syncGET:urlString];
    //NSLog(@"%@", [groupArray description]);

    for (NSDictionary *dataDict in groupArray)
    {
        NSString *tid = [dataDict objectForKey:@"tid"];
        NSString *dic_images = [dataDict objectForKey:@"field_img_cover"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL  *url = [NSURL URLWithString:dic_images];
            NSData *urlData = [NSData dataWithContentsOfURL:url];
            
            if ( urlData )
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                
                NSString *filenameOnly = [dic_images lastPathComponent];
                NSString *filePath = [NSString stringWithFormat:@"%@/%@_%@", documentsDirectory,tid,filenameOnly];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [urlData writeToFile:filePath atomically:YES];
                    [_tableView reloadData];
                });
            }
        });
    }
}


-(void)getBanner
{
    NSString *urlString = [NSString stringWithFormat:@"service/banner/%@", cellTid];
    NSLog(@"%@",urlString);
    
    bannerArray = [RESTAPI syncGET:urlString];
    
    int i = 0;
    for (NSDictionary *bannerDic in bannerArray)
    {
        NSString *dic_banner_image = [bannerDic objectForKey:@"field_banner_image"];
        
        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                  [NSURL URLWithString:dic_banner_image]]];

        UIButton * tagBanner = [[UIButton alloc] initWithFrame:CGRectMake(i*self.scrollView.frame.size.width,
                                                                          0, self.scrollView.frame.size.width,
                                                                          self.scrollView.frame.size.height)];
        tagBanner.tag = i ;
        
        [tagBanner addTarget:self
                      action:@selector(methodTouchUpInside:)
            forControlEvents:UIControlEventTouchUpInside];
        
        [tagBanner setImage:image forState:UIControlStateNormal];
        [self.scrollView addSubview:tagBanner];
        
        i++;
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * [bannerArray count],
                                             self.scrollView.frame.size.height-100);
    
    pageControl.currentPage = 0;
    pageControl.numberOfPages = [bannerArray count];
    scrollView.pagingEnabled = YES;
    
    /*
    UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                           action:@selector(methodName:)];
    [tap setNumberOfTapsRequired:1];
    [tap setNumberOfTouchesRequired:2];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:tap];
     */
    
}

- (void)methodTouchUpInside:(UIButton*)sender
{
     NSLog(@"TouchUpInside sender %ld",(long)sender.tag);
    
    ClassSubLessonViewController *classSubLessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassSubLessonViewController"];
    NSDictionary *groupDic = [groupArray objectAtIndex:sender.tag];
    
    classSubLessonView.tid = [groupDic objectForKey:@"tid"];
    classSubLessonView.name = [groupDic objectForKey:@"name"];
    classSubLessonView.pdf = [groupDic objectForKey:@"field_pdf"];
    
    classSubLessonView.cellName = cellName;
    
    [self.navigationController pushViewController:classSubLessonView animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return groupArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"BG4"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"BG4"];
    } else {
        background = [UIImage imageNamed:@"BG4"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"classlessonCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    NSDictionary *groupDic = [groupArray objectAtIndex:indexPath.row];
    
    UIImageView *clipImageView = (UIImageView *)[cell viewWithTag:101];
    
    UILabel *title_tag = (UILabel *)[cell viewWithTag:103];
    UILabel *tid_tag = (UILabel *)[cell viewWithTag:104];
    UILabel *viewed_tag = (UILabel *)[cell viewWithTag:105];
    UIButton *watchlater_tag = (UIButton *)[cell viewWithTag:106];
    
    [watchlater_tag setUserInteractionEnabled:YES];
//    [watchlater_tag addTarget:self
//                       action:@selector(postWatchLater:)
//             forControlEvents:UIControlEventTouchUpInside];
//    watchlater_tag.tag = indexPath.row;
    
    [watchlater_tag setSelected:NO];
    NSString *status = [[groupDic valueForKey:@"field_watch_later"] stringValue];
    
    if([status isEqualToString:@"1"])
    {
        [watchlater_tag setSelected:YES];
    }
    
    title_tag.text = [groupDic objectForKey:@"name"];
    title_tag.minimumScaleFactor = 0.2;
    title_tag.numberOfLines = 0;
    //[title_tag sizeToFit];

    NSString *titleGruop =  [groupDic objectForKey:@"name"];
    CGSize maximumLabelSize = CGSizeMake(200,100);
    CGSize expectedLabelSize = [titleGruop sizeWithFont:title_tag.font
                                 constrainedToSize:maximumLabelSize
                                     lineBreakMode:NSLineBreakByWordWrapping];
    
    CGRect newFrame = title_tag.frame;
    newFrame.size.height = expectedLabelSize.height;
    title_tag.frame = newFrame;
    
    
    tid_tag.text = [groupDic objectForKey:@"tid"];
    
    if ([[groupDic objectForKey:@"field_people_viewed"] isEqual: @"0"])
    {
        viewed_tag.text = [NSString stringWithFormat:@"1 views"];
    }
    else
    {
        viewed_tag.text = [NSString stringWithFormat:@"%@ views", [groupDic objectForKey:@"field_people_viewed"]];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filenameOnly = [groupDic objectForKey:@"field_img_cover"];
    NSString *tid = [groupDic objectForKey:@"tid"];
    filenameOnly = [filenameOnly lastPathComponent];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@_%@", documentsDirectory,tid,filenameOnly];
    
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    clipImageView.contentMode = UIViewContentModeScaleAspectFit;
    clipImageView.image = image;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClassSubLessonViewController *classSubLessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassSubLessonViewController"];
    NSDictionary *groupDic = [groupArray objectAtIndex:indexPath.row];
    
    classSubLessonView.tid = [groupDic objectForKey:@"tid"];
    classSubLessonView.name = [groupDic objectForKey:@"name"];
    classSubLessonView.pdf = [groupDic objectForKey:@"field_pdf"];
    
    classSubLessonView.cellName = cellName;

    [self.navigationController pushViewController:classSubLessonView animated:YES];
}

- (IBAction)sortby:(id)sender
{    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    alert.backgroundViewColor = [UIColor whiteColor];
    
    [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:0.0f];
    [alert setBodyTextFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
    [alert setButtonsTextFontFamily:@"DBSaiKrokX" withSize:20.0f];
    
    
    [alert addButton:@"SORT BY NAME"
              target:self
            selector:@selector(byName)];
    
    [alert addButton:@"SORT BY WATCH LATER"
              target:self
            selector:@selector(byWatchLater)];
    
    [alert showCustom:self
                image:[UIImage imageNamed:@"app_01"]
                color:Color
                title:@"sort by"
             subTitle:@"     "
     closeButtonTitle:@"CLOSE"
             duration:0.0f];
}

-(void)byName
{
    groupArray = [groupArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *g1, NSDictionary *g2) {
        return [[g1 objectForKey:@"name"] compare:[g2 objectForKey:@"name"] ];
    }];
    [self.tableView reloadData];
}

-(void)byWatchLater
{
    groupArray = [groupArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *g1, NSDictionary *g2) {
        return [[g1 objectForKey:@"watch_later"] compare:[g2 objectForKey:@"watch_later"] ];
        
    }];
     [self.tableView reloadData];
}

-(void)scrollViewDidScroll:(UIScrollView *)localScrollView
{
    pageControl.currentPage = localScrollView.contentOffset.x/self.view.frame.size.width;
}


- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
