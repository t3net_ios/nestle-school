//
//  ClassSubClipViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/30/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

#import <Social/Social.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface ClassSubClipViewController : UIViewController <YTPlayerViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *playlistArray;
    NSDictionary *dictionary;
    NSString *uid;
    
    NSDictionary *commentDic;
    NSArray *commentArray;
    NSMutableArray *commentTypeArray;
    
    NSString *field_nid;
    NSString *title;
    NSString *body;
    NSString *field_images;
    NSString *field_playlist;
    NSString *field_youtube;
    NSString *field_class;
    NSString *field_subjects;
    NSString *field_group_lessons;
    
    NSString *base_url;
    NSString *field_pdf;
    NSString *field_tag;
    NSString *field_length_vdo;
    NSString *field_see_vdo;
    NSString *field_people_viewed;
    NSString *field_group_lessons_id;
    
    NSString *lesson_nid;
    NSString *lesson_tid;
    
    NSInteger currentVideoIndex;
    BOOL isPlaylist;
    NSArray *videos;
    NSDictionary *playerVars;
    NSDictionary *currentVideo;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;

@property (retain)UIDocumentInteractionController *docController;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UILabel *classSubCliplbl;

@property (weak, nonatomic) IBOutlet UILabel *grouplessonslbl;
@property (weak, nonatomic) IBOutlet UILabel *seeingvdolbl;
@property (weak, nonatomic) IBOutlet UILabel *ppviewedlbl;

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (nonatomic, strong) NSString *tidClassSubClip;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *tid;
@property (nonatomic, strong) NSString *nid;

@property (nonatomic)  BOOL isPlaylist ;
@property (nonatomic)  NSInteger currentVideoIndex;
@property (nonatomic, strong) NSArray *videos;

@property (weak, nonatomic) IBOutlet UITextField *commentTxt;

- (IBAction)shaerd:(id)sender;

- (IBAction)back:(id)sender;



@end
