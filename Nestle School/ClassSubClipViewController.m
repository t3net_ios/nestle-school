//
//  ClassSubClipViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/30/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ClassSubClipViewController.h"
#import "RESTAPI.h"

#import "TestSubjectViewController.h"

#import "YTPlayerView.h"
#import "SCLAlertView.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface ClassSubClipViewController ()
@end

@implementation ClassSubClipViewController

@synthesize classSubCliplbl;

@synthesize tidClassSubClip, name, tid;

@synthesize grouplessonslbl, seeingvdolbl, ppviewedlbl;

@synthesize profileImage;

@synthesize docController;

@synthesize currentVideoIndex;
@synthesize isPlaylist ;
@synthesize videos;

@synthesize commentTxt;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    classSubCliplbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    classSubCliplbl.layer.shadowOffset = CGSizeMake(0,1);
    classSubCliplbl.layer.shadowOpacity = 1.0;
    classSubCliplbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    NSDictionary *profileData = [[NSUserDefaults standardUserDefaults] valueForKey:@"profile"];
    uid = [profileData objectForKey:@"uid"];
    
    NSString *urlPicture = [NSString stringWithFormat:@"%@",[profileData objectForKey:@"picture"]];
    NSURL *url = [NSURL URLWithString:urlPicture];
    NSData *dataImage = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:dataImage];
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.layer.borderWidth = 3.0f;
    self.profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileImage.clipsToBounds = YES;
    
    [self.profileImage setImage:image];

    self.playerView.delegate = self ;
    
    if(isPlaylist == YES) {
        
        playerVars = @{
                       @"cc_load_policy" : @1,
                       @"playsinline" : @1,
                       @"autoplay" : @1,
                       @"showinfo" : @1,
                       @"autohide" : @1,
                       @"rel" : @0,
                       @"modestbranding" : @1
                       };
    } else {
        
        playerVars = @{ @"cc_load_policy" : @1,
                        @"playsinline" : @1,
                        @"autoplay" : @0,
                        @"showinfo" : @1,
                        @"autohide" : @1,
                        @"rel" : @0,
                        @"modestbranding" : @1 };
    }

    [self playvideoWithIndex:currentVideoIndex];
    [self getComment];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    [refresh addTarget:self action:@selector(refreshTableview:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refresh];

}

- (void)refreshTableview:(UIRefreshControl*)refresh
{
    [self getComment];
    [refresh endRefreshing];
}

-(void)playvideoWithIndex:(NSInteger)index
{
    currentVideo = [videos objectAtIndex:index];
    tidClassSubClip = [currentVideo objectForKey:@"nid"];
    lesson_nid = tidClassSubClip;
    lesson_tid = tid;

    [self postPeopleviewed];
}

-(void)getServiceData
{
    NSString *urlString = [NSString stringWithFormat:@"service/lesson?nid=%@",lesson_nid];
    NSLog(@"%@",urlString);
    
    NSArray *result = [RESTAPI syncGET:urlString];

    for (NSDictionary *dataDict in result)
    {
        field_nid = [dataDict objectForKey:@"nid"];
        title = [dataDict objectForKey:@"title"];
        
        field_images = [dataDict objectForKey:@"field_images"];
        field_youtube = [dataDict objectForKey:@"field_youtube"];
        
        field_class = [dataDict objectForKey:@"field_class"];
        field_subjects = [dataDict objectForKey:@"field_subjects"];
        
        field_pdf = [dataDict objectForKey:@"field_pdf"];
        field_see_vdo = [dataDict objectForKey:@"field_see_vdo"];
        field_people_viewed =  [dataDict objectForKey:@"field_people_viewed"];
        
        classSubCliplbl.text = name;
        grouplessonslbl.text = title;
        //grouplessonslbl.numberOfLines = 0;
        //[grouplessonslbl sizeToFit];

        
        if ([field_see_vdo  isEqual: @"0"])
        {
            seeingvdolbl.text = [NSString stringWithFormat:@"มีคนเข้าเรียนอยู่  1  คน"];
            
        } else {
            
            seeingvdolbl.text = [NSString stringWithFormat:@"มีคนเข้าเรียนอยู่  %@  คน", field_see_vdo];
        }
        
        if ([field_people_viewed  isEqual: @""])
        {
            ppviewedlbl.text = [NSString stringWithFormat:@"1  VIEWS"];
            
        } else {
            
            ppviewedlbl.text = [NSString stringWithFormat:@"%@  VIEWS", field_people_viewed];
        }
    }
    
    [self.playerView loadWithVideoId:field_youtube playerVars:playerVars];
}

-(void)postPeopleviewed
{
    NSDictionary *datauser = @{ @"nid": lesson_nid,
                                @"tid": lesson_tid };
    
    NSDictionary *result = [RESTAPI syncPOST:@"service/peopleviewed" parameters:datauser];
    
    if (result)
    {
        [self postSeeVDO];
    }
}

-(void)postSeeVDO
{
    NSDictionary *datauser = @{ @"nid": lesson_nid,
                                @"action": @"play" };

    NSLog(@"postSeeVDO %@",[datauser description]);
    NSDictionary *result = [RESTAPI syncPOST:@"service/seevdo" parameters:datauser];
    
    if (result)
    {
        NSLog(@"PLAY");
        [self getServiceData];
    }
}

-(void)postStopVDO
{
    NSDictionary *datauser = @{ @"nid": lesson_nid,
                                @"action": @"stop" };
    NSLog(@"postStopVDO %@",[datauser description]);
    NSDictionary *result = [RESTAPI syncPOST:@"service/seevdo" parameters:datauser];
    
    if (result)
    {
        NSLog(@"STOP");
    }
}

-(void)postComment
{
    NSDictionary *datauser = @{ @"nid": lesson_nid,
                                @"uid": uid,
                                @"subject": field_subjects,
                                @"body": commentTxt.text,
                                @"type" : @"1"};
    
    NSLog(@"postComment %@",[datauser description]);
    NSDictionary *result = [RESTAPI syncPOST:@"service/addcomment" parameters:datauser];
    
    if (result)
    {
        NSLog(@"%@", [result description]);
    }
}

-(void)getComment
{
    NSString *urlString = [NSString stringWithFormat:@"service/comment-node/%@", lesson_nid];
    NSLog(@"%@",urlString);
    
    NSArray *result = [RESTAPI syncGET:urlString];
    
    if ([result count] == 0)
    {
        NSLog(@"NULL");
        
    } else {

        NSString *comment_type = @"" ;
        NSMutableArray *typeArray = [NSMutableArray new];
        
        BOOL isFirstObject = YES;
        
        for (NSDictionary *dataDict in result){
            NSString *nid = [dataDict valueForKey:@"nid"];
            NSString *pid = [dataDict valueForKey:@"pid"];
            NSString *cid = [dataDict valueForKey:@"cid"];
            NSString *dic_title = [dataDict valueForKey:@"title"];
            NSString *comment_body = [dataDict valueForKey:@"comment_body"];
            NSString *user = [dataDict valueForKey:@"user"];
            NSString *dic_comment_type = [dataDict valueForKey:@"comment_type"];
            
            dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                          nid, @"nid",
                          pid, @"pid",
                          cid, @"cid",
                          dic_title, @"title",
                          comment_body, @"comment_body",
                          user, @"user",
                          dic_comment_type, @"dic_comment_type",
                          nil];
            
            [typeArray addObject:dictionary];

            //First Object
            if(isFirstObject){
                isFirstObject = NO ;
                comment_type = dic_comment_type;
            }
            
            //If Change Type
            if(![comment_type isEqualToString:dic_comment_type]){
                comment_type = dic_comment_type;
                
                [commentTypeArray addObject:[typeArray copy]];
                
                [typeArray removeAllObjects];
                
            }
        }
        
        //Add Last Object
        [commentTypeArray addObject:[typeArray copy]];
    }
}

- (IBAction)shaerd:(id)sender
{
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:field_images]]];
    
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = image;
    photo.userGenerated = YES;
    
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:nil];

}

- (IBAction)back:(id)sender
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    alert.backgroundViewColor = [UIColor whiteColor];
    
    [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
    [alert setBodyTextFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
    [alert setButtonsTextFontFamily:@"DBSaiKrokX" withSize:20.0f];
    
    [alert addButton:@"โดดเรียน"
              target:self
            selector:@selector(yes)];
    
    [alert showCustom:self
                image:[UIImage imageNamed:@"app_01"]
                color:Color
                title:@"แหนะๆ ครูเห็นนะ"
             subTitle:@"จะหนีไปไหน ยังไม่หมดคาบเลย"
     closeButtonTitle:@"เรียนต่อ"
             duration:0.0f];
}

- (void)yes
{
    [self.playerView stopVideo];
    [self postStopVDO];

    [self.navigationController popViewControllerAnimated:NO];
}

-(void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state
{
     if (state == kYTPlayerStatePlaying)
     {
        NSLog(@"Start Video");
         
     } else if (state == kYTPlayerStateEnded){
         
         NSLog(@"End Video");
         
         if(isPlaylist == YES)
         {
             currentVideoIndex++;
             [self playvideoWithIndex:currentVideoIndex];
             
         }
     }
}

-(void)playerViewDidBecomeReady:(YTPlayerView *)playerView
{
    if(isPlaylist){
        [self.playerView playVideo];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return commentArray.count;
    return commentTypeArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"BG4"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"BG4"];
    } else {
        background = [UIImage imageNamed:@"BG4"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"commentCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    commentDic = [commentTypeArray objectAtIndex:indexPath.row];
    
    //UIImageView *clipImageView = (UIImageView *)[cell viewWithTag:101];
    UILabel *usertxt = (UILabel *)[cell viewWithTag:102];
    UILabel *commenttxt = (UILabel *)[cell viewWithTag:103];
    
    NSString *commentStr = [commentDic objectForKey:@"comment_body"];
    
    commentStr = [commentStr stringByReplacingOccurrencesOfString:@"<p>" withString:@" "];
    commentStr = [commentStr stringByReplacingOccurrencesOfString:@"</p>" withString:@" "];

    usertxt.text = [commentDic objectForKey:@"user"];
    commenttxt.text = commentStr;
    
    return cell;
}


- (IBAction)askAction:(id)sender
{
    NSLog(@"1");
    
    [_tableView reloadData];
}

- (IBAction)commentAction:(id)sender
{
    NSLog(@"2");

    [self.tableView reloadData];
}


- (IBAction)textFieldDismiss:(id)sender
{
    if (commentTxt.text.length > 0)
    {
        [self postComment];
        self.commentTxt.text = @"";
    }
    
    [commentTxt resignFirstResponder];
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}



@end
