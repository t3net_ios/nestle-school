//
//  ClassLessonViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/30/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassSubLessonViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSDictionary *profileData;
    
    NSDictionary *listLessonDic;
    NSArray *listLessonArray;
    NSMutableArray *watchLaterArray;

    NSString *nid;
    NSString *uid;
    NSString *title;
}
@property (retain)UIDocumentInteractionController *docController;

@property (weak, nonatomic) IBOutlet UILabel *classSubLessonlbl;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSString *cellName;

@property (nonatomic, strong) NSString *tid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *pdf;

- (void)refreshTableview:(UIRefreshControl*)refresh;

- (IBAction)downloadPDF:(id)sender;

- (IBAction)playAllbtn:(id)sender;

- (IBAction)back:(id)sender;

@end

