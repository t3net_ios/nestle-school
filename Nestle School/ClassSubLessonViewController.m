//
//  ClassLessonViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/30/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ClassSubLessonViewController.h"
#import "RESTAPI.h"

#import "ClassSubClipViewController.h"

@interface ClassSubLessonViewController ()
@end

@implementation ClassSubLessonViewController

@synthesize classSubLessonlbl;

@synthesize docController;

@synthesize cellName;
@synthesize tid, name, pdf;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    classSubLessonlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    classSubLessonlbl.layer.shadowOffset = CGSizeMake(0, 1);
    classSubLessonlbl.layer.shadowOpacity = 1.0;
    classSubLessonlbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    profileData = [[NSUserDefaults standardUserDefaults] valueForKey:@"profile"];
    uid = [profileData objectForKey:@"uid"];
    
    classSubLessonlbl.text = name;
    
    [self getListLesson];
    
    
    
//    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
//    [refresh addTarget:self action:@selector(refreshTableview:) forControlEvents:UIControlEventValueChanged];
//    [self.tableView addSubview:refresh];
}

- (void)getListLesson
{
    NSString *urlString = [NSString stringWithFormat:@"service/list-lesson/%@/%@", tid, uid];
    NSLog(@"%@",urlString);
    
    listLessonArray = [RESTAPI syncGET:urlString];
    watchLaterArray = [NSMutableArray new];
    
    for (NSDictionary *dataDict in listLessonArray)
    {
        title = [dataDict objectForKey:@"title"];
        NSString *dic_images = [dataDict objectForKey:@"field_images"];
        
        NSString *dic_watch_later = [dataDict objectForKey:@"field_watch_later"];
        NSData *data = [dic_watch_later dataUsingEncoding:NSUTF8StringEncoding];
        
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSArray *listField_watch_later = (NSArray*)json;
        
        if(listField_watch_later.count > 0) {
            
            for(NSDictionary *objectField_watch_later in listField_watch_later)
            {
                NSString *status = [objectField_watch_later objectForKey:@"status"];
                
                if([status isEqualToString:@"1"])
                {
                    [watchLaterArray addObject:@"1"];
                }
            }
            
        } else {
            
            [watchLaterArray addObject:@"0"];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL  *url = [NSURL URLWithString:dic_images];
            NSData *urlData = [NSData dataWithContentsOfURL:url];
            
            if ( urlData )
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                
                //Get only image name;
                NSString *filenameOnly = [dic_images lastPathComponent];
                NSString *filePath = [NSString stringWithFormat:@"%@/%@_%@", documentsDirectory,tid,filenameOnly];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [urlData writeToFile:filePath atomically:YES];
                    [_tableView reloadData];
                });
            }
            
        });
    }
}


- (IBAction)downloadPDF:(id)sender
{
    NSString *currentURL = [NSString stringWithFormat:@"%@",pdf];
    
    //Download PDF
    __block NSData *data ;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:currentURL]];
    
    NSURLConnection *conn = [[NSURLConnection alloc] init];
    (void) [conn initWithRequest:request delegate:self];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        data = [NSData dataWithContentsOfURL:[NSURL URLWithString:currentURL]];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *namePDF = [NSString stringWithFormat:@"%@%@.pdf",title,name];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:namePDF];
        
        NSError *error ;
        
        BOOL status = [data writeToFile:filePath options:NSDataWritingAtomic error:&error];
        
        if(!status)
        {
            NSLog(@"Error");
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSURL *targetURL = [NSURL fileURLWithPath:filePath];
            
            docController = [UIDocumentInteractionController interactionControllerWithURL:targetURL];
            
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"itms-bookss:"]])
            {
                
                [docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
                
            } else {
                
                NSLog(@"iBooks not installed");
            }
        });
    });
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listLessonArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"BG4"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"BG4"];
    } else {
        background = [UIImage imageNamed:@"BG4"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"classsublessonCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    listLessonDic = [listLessonArray objectAtIndex:indexPath.row];
    
    UIImageView *clipImageView = (UIImageView *)[cell viewWithTag:101];
    
    UILabel *title_tag = (UILabel *)[cell viewWithTag:103];
    UILabel *nid_tag = (UILabel *)[cell viewWithTag:104];
    UILabel *viewed_tag = (UILabel *)[cell viewWithTag:105];
    UIButton *watchlater_tag = (UIButton *)[cell viewWithTag:106];
    
    [watchlater_tag setUserInteractionEnabled:YES];
    [watchlater_tag addTarget:self
                       action:@selector(postWatchLater:)
             forControlEvents:UIControlEventTouchUpInside];
    watchlater_tag.tag = indexPath.row;

    [watchlater_tag setSelected:NO];
    
    NSString *status = [watchLaterArray objectAtIndex:indexPath.row];
    if([status isEqualToString:@"1"])
    {
        [watchlater_tag setSelected:YES];
    }

    title_tag.text = [listLessonDic objectForKey:@"title"];
    title_tag.minimumScaleFactor = 0.2;
    title_tag.numberOfLines = 0;
    //[title_tag sizeToFit];
    
    NSString *titleLesson =  [listLessonDic objectForKey:@"title"];
    CGSize maximumLabelSize = CGSizeMake(200,100);
    CGSize expectedLabelSize = [titleLesson sizeWithFont:title_tag.font
                                 constrainedToSize:maximumLabelSize
                                     lineBreakMode:NSLineBreakByWordWrapping];
    
    CGRect newFrame = title_tag.frame;
    newFrame.size.height = expectedLabelSize.height;
    title_tag.frame = newFrame;
    
    nid_tag.text = [listLessonDic objectForKey:@"nid"];
    
    if ([[listLessonDic objectForKey:@"field_people_viewed"] isEqual: @""])
    {
        viewed_tag.text = [NSString stringWithFormat:@"1 views"];
    }
    else
    {
        viewed_tag.text = [NSString stringWithFormat:@"%@ views", [listLessonDic objectForKey:@"field_people_viewed"]];
    }
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filenameOnly = [listLessonDic objectForKey:@"field_images"];
    filenameOnly = [filenameOnly lastPathComponent];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@_%@", documentsDirectory,tid,filenameOnly];
    
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    clipImageView.image = image;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self startPlayAtIndex:indexPath.row isPlaylist:NO];
}

-(void)postWatchLater:(UIButton*)sender
{
    UIButton *senderButton = (UIButton *)sender;
    NSInteger currentRow = senderButton.tag;
    NSLog(@"currentRow = %ld",(long)sender.tag);
    
    
    NSString *lesson_id = [listLessonArray[currentRow] objectForKey:@"nid"];
    
    NSDictionary *datauser = @{ @"uid": uid,
                                @"lessonId": lesson_id };
    
    NSDictionary *result = [RESTAPI syncPOST:@"service/watchlater"
                                  parameters:datauser];
    
    if ([senderButton isSelected])
    {
        [watchLaterArray replaceObjectAtIndex:currentRow withObject:@"0"];
        [senderButton setSelected:NO];
   
    } else {
        [watchLaterArray replaceObjectAtIndex:currentRow withObject:@"1"];
        [senderButton setSelected:YES];
    }
}

- (IBAction)playAllbtn:(id)sender
{
    [self startPlayAtIndex:0 isPlaylist:YES];
}

-(void)startPlayAtIndex:(NSInteger)index isPlaylist:(BOOL)playlist
{
    ClassSubClipViewController *classSubClipView = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassSubClipViewController"];
    
    classSubClipView.isPlaylist = playlist;
    classSubClipView.videos = [listLessonArray copy];
    classSubClipView.name = name;
    classSubClipView.tid = tid;
    classSubClipView.nid = nid;
    classSubClipView.currentVideoIndex = index;
    
    [self.navigationController pushViewController:classSubClipView animated:YES];
}



- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}



@end
