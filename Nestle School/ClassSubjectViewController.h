//
//  ClassSubjectViewController.h
//  Nestle School
//
//  Created by MOREMO on 10/13/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassSubjectViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSDictionary *subjectDic;
    NSArray *subjectArray;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *classSublbl;
@property (weak, nonatomic) IBOutlet UILabel *class_idCS;

@property (nonatomic, strong) NSString *cellName;
@property (nonatomic, strong) NSString *cellTid;

- (IBAction)back:(id)sender;

@end
