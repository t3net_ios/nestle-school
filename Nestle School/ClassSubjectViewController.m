//
//  ClassSubjectViewController.m
//  Nestle School
//
//  Created by MOREMO on 10/13/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ClassSubjectViewController.h"
#import "RESTAPI.h"

#import "ClassLessonViewController.h"

@interface ClassSubjectViewController ()
@end

@implementation ClassSubjectViewController

@synthesize classSublbl;
@synthesize class_idCS;
@synthesize cellName, cellTid;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    classSublbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    classSublbl.layer.shadowOffset = CGSizeMake(0, 1);
    classSublbl.layer.shadowOpacity = 1.0;
    classSublbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    class_idCS.text = cellName;
    
    [self getSubjcet];
}

- (void)getSubjcet
{
    NSString *urlString = [NSString stringWithFormat:@"service/subjects?class_id=%@",cellTid];
    NSLog(@"%@",urlString);
    
    subjectArray = [RESTAPI syncGET:urlString];
    //NSLog(@"%@", [subjectArray description]);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return subjectArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"app_23"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"app_23"];
    } else {
        background = [UIImage imageNamed:@"app_23"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"classsubjectCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    subjectDic = [subjectArray objectAtIndex:indexPath.row];
    
    UILabel *name_tag = (UILabel *)[cell viewWithTag:101];
    UILabel *tid_tag = (UILabel *)[cell viewWithTag:102];
    
    name_tag.text = [subjectDic objectForKey:@"name"];
    tid_tag.text = [subjectDic objectForKey:@"tid"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClassLessonViewController *classLessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassLessonViewController"];
    
    subjectDic = [subjectArray objectAtIndex:indexPath.row];
    
    classLessonView.cellName = [subjectDic objectForKey:@"name"];
    classLessonView.cellTid = [subjectDic objectForKey:@"tid"];

    [self.navigationController pushViewController:classLessonView animated:YES];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
