//
//  ClassViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ClassViewController.h"
#import "RESTAPI.h"

#import "ClassSubjectViewController.h"
#import "ClassLessonViewController.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]


@interface ClassViewController ()
@end

@implementation ClassViewController

@synthesize classlbl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    classlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    classlbl.layer.shadowOffset = CGSizeMake(0, 1);
    classlbl.layer.shadowOpacity = 1.0;
    classlbl.layer.shadowRadius = 1.0;

    
    //////////////////////////////////////////////////
    [self getClassroom];
}

- (void)getClassroom
{
    NSString *urlString = [NSString stringWithFormat:@"service/class-rooms"];
    NSLog(@"%@",urlString);
    
    classRoomArray = [RESTAPI syncGET:urlString];
    subjectArray = [NSMutableArray new];
    displayRows = [NSMutableArray new];
    
    //Get Lesson
    for(NSDictionary *class in classRoomArray)
    {
        NSString *urlString = [NSString stringWithFormat:@"service/subjects?class_id=%@",[class objectForKey:@"tid"]];
        //NSLog(@"%@",urlString);
        
        NSArray *subjects = [RESTAPI syncGET:urlString];
        [subjectArray addObject:subjects];
        
        [displayRows addObject:@(0)];
    }
    //NSLog(@"%@", [classRoomArray description]);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return classRoomArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[displayRows objectAtIndex:section] integerValue];
}

-(NSArray*) indexPathsForSection:(NSInteger)section withNumberOfRows:(NSInteger)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (NSInteger i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}

-(void)sectionButtonTouchUpInside:(UIButton*)sender
{
     NSInteger section = sender.tag;
    
    /*
    if(lastSelectedButton){
        if(lastSelectedButton.tag != sender.tag){
            [sender setBackgroundImage:[UIImage imageNamed:@"app_19 copy"] forState:UIControlStateNormal];
            
            int numOfRows = [self.tableView numberOfRowsInSection:lastSelectedButton.tag];
            NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [displayRows replaceObjectAtIndex:lastSelectedButton.tag withObject:@(0)];
        }
    }
    lastSelectedButton = sender;
     */
    
    [sender setSelected:!sender.selected];
    
    [self.tableView beginUpdates];
   
    BOOL shouldCollapse = !sender.selected;
    if (shouldCollapse) {
        [sender setBackgroundImage:[UIImage imageNamed:@"app_19 copy"] forState:UIControlStateNormal];
        
        NSInteger numOfRows = [self.tableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [displayRows replaceObjectAtIndex:section withObject:@(0)];
    }
    else {
        [sender setBackgroundImage:[UIImage imageNamed:@"app_22"] forState:UIControlStateNormal];
        
        NSInteger numOfRows = [subjectArray[section] count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [displayRows replaceObjectAtIndex:section withObject:@(numOfRows)];
    }
    [self.tableView endUpdates];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 67;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67+10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
    [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    result.backgroundColor = [UIColor clearColor];
    result.adjustsImageWhenHighlighted = NO;
    [result setBackgroundImage:[UIImage imageNamed:@"app_19 copy"] forState:UIControlStateNormal];

    NSString *title = [[classRoomArray objectAtIndex:section] valueForKey:@"name"];
    [result setTitle:title forState:UIControlStateNormal];
    
    [result.titleLabel setFont:[UIFont fontWithName:@"DBSaiKrokX" size:40.0f]];
    [result setTitleColor:Color forState:UIControlStateNormal];
    result.tag = section;
    
    return result;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:indexPath.section];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    

    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"app_23"];
    } else if (rowIndex == rowCount - 1) {
        //Last Cell
        background = [UIImage imageNamed:@"app_24"];
    } else {
        background = [UIImage imageNamed:@"app_23"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"classCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    NSArray *subjects = [subjectArray objectAtIndex:indexPath.section];
    NSDictionary *subject = [subjects objectAtIndex:indexPath.row];

    UILabel *name_tag = (UILabel *)[cell viewWithTag:101];
    UILabel *tid_tag = (UILabel *)[cell viewWithTag:102];
    
    name_tag.text = [subject objectForKey:@"name"];
    [name_tag setTextColor:Color];
    tid_tag.text = [subject objectForKey:@"tid"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClassLessonViewController *classLessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassLessonViewController"];
    
    NSArray *subjects = [subjectArray objectAtIndex:indexPath.section];
    NSDictionary *subject = [subjects objectAtIndex:indexPath.row];
    
    classLessonView.cellName = [subject objectForKey:@"name"];
    classLessonView.cellTid = [subject objectForKey:@"tid"];

    [self.navigationController pushViewController:classLessonView animated:YES];
}

@end
