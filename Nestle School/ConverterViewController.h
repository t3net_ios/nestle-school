//
//  ConverterViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConverterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *converterArray;
    NSDictionary *dictionary;
    
    NSString *title;
    NSString *nid;
}

@property (weak, nonatomic) IBOutlet UILabel *converterlbl;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSString *tidConverter;

- (IBAction)back:(id)sender;

@end
