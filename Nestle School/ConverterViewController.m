//
//  ConverterViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ConverterViewController.h"

#import "RESTAPI.h"

@interface ConverterViewController ()
@end

@implementation ConverterViewController

@synthesize converterlbl;

@synthesize tidConverter;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    converterlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    converterlbl.layer.shadowOffset = CGSizeMake(0, 1);
    converterlbl.layer.shadowOpacity = 1.0;
    converterlbl.layer.shadowRadius = 1.0;
    
    NSString *typeId = tidConverter;
    
    converterArray = [[NSMutableArray alloc] init];
    
    title = @"title";
    nid = @"nid";
    
    NSString *urlString = [NSString stringWithFormat:@"service/list-help?typeId=%@",typeId];
    NSLog(@"%@",urlString);
    
    NSDictionary *result = [RESTAPI syncGET:urlString];
    
    for (NSDictionary *dataDict in result)
    {
        NSString *str_title = [dataDict objectForKey:@"title"];
        NSString *str_nid = [dataDict objectForKey:@"nid"];
        
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                      str_title, title,
                      str_nid, nid,
                      nil];
        
        [converterArray addObject:dictionary];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return converterArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"app_19 copy"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"app_19 copy"];
    } else {
        background = [UIImage imageNamed:@"app_19 copy"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"converterCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    NSDictionary *tmpDict = [converterArray objectAtIndex:indexPath.row];
    
    UILabel *titletxt = (UILabel *)[cell viewWithTag:101];
    titletxt.text = [tmpDict objectForKey:title];
    
    return cell;
}


- (IBAction)back:(id)sender
{
    //[self dismissViewControllerAnimated:NO completion:^{
        //
    //}];
    
    [self.navigationController popViewControllerAnimated:NO];
}

@end
