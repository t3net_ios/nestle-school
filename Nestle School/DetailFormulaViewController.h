//
//  DetailFormulaViewController.h
//  Nestle School
//
//  Created by MMOREMO on 11/19/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailFormulaViewController : UIViewController <UIScrollViewDelegate>
{
    NSArray *detailformulaArray;
    
    NSArray *relatedArray;
}

@property (weak, nonatomic) IBOutlet UILabel *detailFormulalbl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollTagView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) NSString *nid;

@property (weak, nonatomic) IBOutlet UILabel *formulaTitle;
@property (weak, nonatomic) IBOutlet UIWebView *myWebView;

- (IBAction)back:(id)sender;

@end
