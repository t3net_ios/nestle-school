//
//  DetailFormulaViewController.m
//  Nestle School
//
//  Created by MMOREMO on 11/19/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "DetailFormulaViewController.h"
#import "RESTAPI.h"

#import "ClassSubLessonViewController.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface DetailFormulaViewController ()
@end

@implementation DetailFormulaViewController

@synthesize detailFormulalbl;
@synthesize nid;

@synthesize formulaTitle;

@synthesize scrollView, scrollTagView;
@synthesize pageControl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    detailFormulalbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    detailFormulalbl.layer.shadowOffset = CGSizeMake(0, 1);
    detailFormulalbl.layer.shadowOpacity = 1.0;
    detailFormulalbl.layer.shadowRadius = 1.0;

    scrollTagView.delegate = self;
    
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = Color;
    
    //////////////////////////////////////////////////
    [self getDetailFormula];
}

- (void)getDetailFormula
{
    NSString *urlString = [NSString stringWithFormat:@"service/formula/%@",nid];
    NSLog(@"%@",urlString);
    
    NSDictionary *detailformulaDic = (NSDictionary*)[RESTAPI syncGET:urlString];
    
    formulaTitle.text = [detailformulaDic objectForKey:@"formula_title"];
    NSString *formula_description = [detailformulaDic objectForKey:@"formula_description"];
    formula_description = [formula_description stringByReplacingOccurrencesOfString:@"/sites/default/file"
                                                                         withString:@"http://128.199.80.37/sites/default/file"];
    
    NSString *imageFile = [self scanString:formula_description startTag:@" src=\""
                                    endTag:@".jpg"];
    if(!imageFile)
    {
        imageFile = [self scanString:formula_description startTag:@" src=\""
                               endTag:@".png"];
    }
    
    [self.myWebView setBackgroundColor:[UIColor clearColor]];
    [self.myWebView loadHTMLString:formula_description baseURL:nil];
    [self.myWebView setBackgroundColor:[UIColor clearColor]];
    [self.myWebView setOpaque:NO];
    //[self.myWebView setScalesPageToFit:NO];
    
    relatedArray = [detailformulaDic objectForKey:@"related_playlists"];
    //NSLog(@"%@",relatedArray);
    
    int i = 0;
    for (NSDictionary *relatedDic in relatedArray)
    {
        NSString *dic_image = [relatedDic objectForKey:@"playlist_image"];
        
        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                  [NSURL URLWithString:dic_image]]];
        
        UIButton *tagBanner = [[UIButton alloc] initWithFrame:CGRectMake(i*self.scrollTagView.frame.size.width/2,
                                                                                0, self.scrollTagView.frame.size.width/2,
                                                                                self.scrollTagView.frame.size.height)];
        tagBanner.tag = i ;
        
        [tagBanner addTarget:self
                      action:@selector(methodTouchUpInside:)
            forControlEvents:UIControlEventTouchUpInside];
        
        [tagBanner setImage:image forState:UIControlStateNormal];
        [self.scrollTagView addSubview:tagBanner];
        
        i++;
    }
    
    self.scrollTagView.contentSize = CGSizeMake(self.scrollTagView.frame.size.width/2 * [relatedArray count],
                                                self.scrollTagView.frame.size.height);

    pageControl.currentPage = 0;
    pageControl.numberOfPages = [relatedArray count]/2;
    scrollTagView.pagingEnabled = YES;
    
    float myWebViewHeight = self.myWebView.frame.size.height;
    float vdotagHeight = self.scrollTagView.frame.size.height;
    float footerOffset = 150;
    //NSLog(@"formulaBodyHeight %f",formulaBodyHeight);
    //[formulaBody setScrollEnabled:NO];
    
    [self.scrollTagView setCenter:CGPointMake(self.view.frame.size.width/2,
                                              myWebViewHeight + footerOffset)];
    
    [self.pageControl setCenter:CGPointMake(self.view.frame.size.width/2,
                                            myWebViewHeight + vdotagHeight + 100)];
}

- (void)methodTouchUpInside:(UIButton*)sender
{
    NSLog(@"TouchUpInside sender %ld",(long)sender.tag);
    
    ClassSubLessonViewController *classSubLessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassSubLessonViewController"];

    NSDictionary *groupDic = [relatedArray objectAtIndex:sender.tag];
    
    classSubLessonView.tid = [groupDic objectForKey:@"playlist_id"];
    classSubLessonView.name = [groupDic objectForKey:@"playlist_title"];
    classSubLessonView.pdf = [groupDic objectForKey:@"playlist_pdf"];
    
    //classSubLessonView.cellName = cellName;
    
    [self.navigationController pushViewController:classSubLessonView animated:YES];
}


-(void)scrollViewDidScroll:(UIScrollView *)localScrollView
{
    pageControl.currentPage = localScrollView.contentOffset.x/self.view.frame.size.width;
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    webView.frame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y, webView.frame.size.width, webView.frame.size.height);
}

- (NSString *)scanString:(NSString *)string
                startTag:(NSString *)startTag
                  endTag:(NSString *)endTag
{
    
    NSString* scanString = @"";
    
    if (string.length > 0) {
        
        NSScanner* scanner = [[NSScanner alloc] initWithString:string];
        
        @try {
            [scanner scanUpToString:startTag intoString:nil];
            scanner.scanLocation += [startTag length];
            [scanner scanUpToString:endTag intoString:&scanString];
        }
        @catch (NSException *exception) {
            return nil;
        }
        @finally {
            return scanString;
        }
        
    }
    
    return scanString;
    
}

@end
