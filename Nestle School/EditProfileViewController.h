//
//  EditProfileViewController.h
//  Nestle School
//
//  Created by MMOREMO on 11/25/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileViewController : UIViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSDictionary *profileData;
    
    NSString *profile_name;
    
    NSArray *prefixArray;
    NSArray *gradeArray;
    NSArray *provinceArray;

    NSString *gradeInt;
}

@property (weak, nonatomic) IBOutlet UILabel *profilelbl;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *profileLevel;

@property (weak, nonatomic) IBOutlet UITextField *usernameTxt;

@property (weak, nonatomic) IBOutlet UITextField *prefixTxt;
@property (weak, nonatomic) IBOutlet UITextField *nameTxt;
@property (weak, nonatomic) IBOutlet UITextField *bdTxt;
@property (weak, nonatomic) IBOutlet UITextField *schoolTxt;
@property (weak, nonatomic) IBOutlet UITextField *gradeTxt;
@property (weak, nonatomic) IBOutlet UITextField *provinceTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;

@property (nonatomic, strong) NSString *edUID;
@property (nonatomic, strong) NSString *edImage;
@property (nonatomic, strong) NSString *edName;
@property (nonatomic, strong) NSString *edLevel;
@property (nonatomic, strong) NSString *edPoint;

@property (nonatomic, strong) NSString *nameImage;
@property (nonatomic, strong) NSString *dataImage;

@property (strong, nonatomic) IBOutlet UIPickerView *dataPicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

-(IBAction)selectSubject:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *selectedSub;
@property (weak, nonatomic) IBOutlet UIButton *selectSub1;
@property (weak, nonatomic) IBOutlet UIButton *selectSub2;
@property (weak, nonatomic) IBOutlet UIButton *selectSub3;
@property (weak, nonatomic) IBOutlet UIButton *selectSub4;
@property (weak, nonatomic) IBOutlet UIButton *selectSub5;
@property (weak, nonatomic) IBOutlet UIButton *selectSub6;
@property (weak, nonatomic) IBOutlet UIButton *selectSub7;
@property (weak, nonatomic) IBOutlet UIButton *selectSub8;
@property (weak, nonatomic) IBOutlet UIButton *selectSub9;

- (IBAction)selectProfile:(id)sender;

- (IBAction)editProfileComplete:(id)sender;

- (IBAction)textFieldDismiss:(id)sender;

- (IBAction)back:(id)sender;
- (IBAction)logout:(id)sender;

@end
