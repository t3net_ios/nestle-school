//
//  EditProfileViewController.m
//  Nestle School
//
//  Created by MMOREMO on 11/25/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "EditProfileViewController.h"
#import "RESTAPI.h"

#import "LoginViewController.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

@synthesize profilelbl;

@synthesize profileImage, profileName, profileLevel;
@synthesize edUID, edImage, edName, edLevel, edPoint;
@synthesize prefixTxt, usernameTxt, nameTxt, bdTxt, schoolTxt, gradeTxt, provinceTxt, emailTxt;
@synthesize nameImage, dataImage;

@synthesize selectedSub;
@synthesize selectSub1, selectSub2, selectSub3, selectSub4, selectSub5, selectSub6, selectSub7, selectSub8, selectSub9;

@synthesize dataPicker, datePicker;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapGesture];

    
    profilelbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    profilelbl.layer.shadowOffset = CGSizeMake(0, 1);
    profilelbl.layer.shadowOpacity = 1.0;
    profilelbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    prefixArray = @[@"กรุณาเลือก", @"เด็กหญิง", @"เด็กชาย", @"นางสาว", @"นาย"];
    gradeArray = @[@"กรุณาเลือก", @"มัธยมศึกษาปีที่ 1", @"มัธยมศึกษาปีที่ 2", @"มัธยมศึกษาปีที่ 3"];
    provinceArray = @[@"กรุณาเลือก", @"กรุงเทพมหานคร", @"กระบี่", @"กาญจนบุรี", @"กาฬสินธุ์", @"กำแพงเพชร", @"ขอนแก่น", @"จันทบุรี", @"ฉะเชิงเทรา", @"ชลบุรี", @"ชัยนาท", @"ชัยภูมิ", @"ชุมพร", @"เชียงราย" ,@"เชียงใหม่", @"ตรัง", @"ตราด", @"ตาก", @"นครนายก", @"นครปฐม", @"นครพนม", @"นครราชสีมา", @"นครศรีธรรมราช", @"นครสวรรค์", @"นนทบุรี", @"นราธิวาส", @"น่าน", @"บึงกาฬ", @"บุรีรัมย์", @"ปทุมธานี", @"ประจวบคีรีขันธ์", @"ปราจีนบุรี", @"ปัตตานี", @"พระนครศรีอยุธยา", @"พังงา", @"พัทลุง", @"พิจิตร", @"พิษณุโลก", @"เพชรบุรี", @"เพชรบูรณ์", @"แพร่", @"พะเยา", @"ภูเก็ต", @"มหาสารคาม", @"มุกดาหาร", @"แม่ฮ่องสอน", @"ยะลา", @"ยโสธร", @"ร้อยเอ็ด", @"ระนอง", @"ระยอง", @"ราชบุรี", @"ลพบุรี", @"ลำปาง", @"ลำพูน", @"เลย", @"ศรีสะเกษ", @"สกลนคร", @"สงขลา", @"สตูล", @"สมุทรปราการ", @"สมุทรสงคราม", @"สมุทรสาคร", @"สระแก้ว", @"สระบุรี", @"สิงห์บุรี", @"สุโขทัย", @"สุพรรณบุรี", @"สุราษฎร์ธานี", @"สุรินทร์", @"หนองคาย", @"หนองบัวลำภู", @"อ่างทอง", @"อุดรธานี", @"อุทัยธานี", @"อุตรดิตถ์", @"อุบลราชธานี", @"อำนาจเจริญ" ];
    
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    [comps setYear:-13];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [comps setYear:-33];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:maxDate];
    [datePicker setMinimumDate:minDate];
    
    [self.bdTxt setInputView:datePicker];
    
    [self.prefixTxt setInputView:dataPicker];
    [self.gradeTxt setInputView:dataPicker];
    [self.provinceTxt setInputView:dataPicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor blackColor]];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                style:UIBarButtonItemStyleDone
                                                               target:self
                                                               action:@selector(selectedDate)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:nil
                                                                           action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn, nil]];
    
    [self.prefixTxt setInputAccessoryView:toolBar];
    [self.gradeTxt setInputAccessoryView:toolBar];
    [self.provinceTxt setInputAccessoryView:toolBar];
    
    [self.bdTxt setInputAccessoryView:toolBar];

    
    profileData = [[NSUserDefaults standardUserDefaults] valueForKey:@"profile"];
    NSLog(@"%@", profileData);
    
    NSString *urlPicture = edImage;
    NSURL *url = [NSURL URLWithString:urlPicture];
    NSData *dataImageData = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:dataImageData];
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.layer.borderWidth = 3.0f;
    self.profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileImage.clipsToBounds = YES;
    
    [self.profileImage setImage:image];
    
    usernameTxt.text = edName;
    prefixTxt.text = [profileData valueForKey:@"prefix"];
    nameTxt.text = [profileData valueForKey:@"fullname"];
    bdTxt.text = [profileData valueForKey:@"birthday"];
    schoolTxt.text = [profileData valueForKey:@"school"];
    gradeTxt.text = [profileData valueForKey:@"class"];
    provinceTxt.text = [profileData valueForKey:@"province"];
    emailTxt.text = [profileData valueForKey:@"mail"];
    
    
    profileLevel.text = edLevel;

}

- (void)selectedDate
{
    if([bdTxt isFirstResponder])
    {
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"MM/dd/yyyy"];
        self.bdTxt.text = [NSString stringWithFormat:@"%@", [dateformatter stringFromDate:datePicker.date]];
        [self.bdTxt resignFirstResponder];
    }
    
    [self.prefixTxt resignFirstResponder];
    [self.gradeTxt resignFirstResponder];
    [self.provinceTxt resignFirstResponder];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)picker
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if([prefixTxt isFirstResponder])
        return prefixArray.count;
    
    if([gradeTxt isFirstResponder])
        return gradeArray.count;
    
    else
        return provinceArray.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([prefixTxt isFirstResponder])
        return [prefixArray objectAtIndex:row];
    
    if([gradeTxt isFirstResponder])
        return [gradeArray objectAtIndex:row];
    
    else
        return [provinceArray objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([prefixTxt isFirstResponder])
    {
        prefixTxt.text = prefixArray[row];
        
    } else if([gradeTxt isFirstResponder]) {
        
        gradeTxt.text = gradeArray[row];
        
    } else {
        
        provinceTxt.text = provinceArray[row];
        
    }
}



/////***CHOOSEPHOTO***/////
- (IBAction)selectProfile:(id)sender
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:@""
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction *takePhoto = [UIAlertAction
                                actionWithTitle:@"Take Photo"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                    picker.delegate = self;
                                    picker.allowsEditing = YES;
                                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                    
                                    [self presentViewController:picker animated:YES completion:NULL];
                                    
                                }];
    
    UIAlertAction *selectPhoto = [UIAlertAction
                                  actionWithTitle:@"Select Photo"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                      picker.delegate = self;
                                      picker.allowsEditing = YES;
                                      picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                      
                                      [self presentViewController:picker animated:YES completion:NULL];
                                      
                                  }];
    
    UIAlertAction *Cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    
    [alert addAction:takePhoto];
    [alert addAction:selectPhoto];
    [alert addAction:Cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    UIImage *resizedImage = [self imageWithImage:image scaledToSize:CGSizeMake(300, 300)];
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.layer.borderWidth = 3.0f;
    self.profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileImage.clipsToBounds = YES;
    
    self.profileImage.image = resizedImage;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
/////***CHOOSEPHOTO***/////



- (void)postLogout
{
    NSDictionary *datauser = @{ @"uid": edUID };
    
    NSDictionary *result = [RESTAPI syncPOST:@"service/logout"
                                  parameters:datauser];
    
    if([result[@"status"] isEqualToString:@"success"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:NO forKey:@"LogOut"];
        [defaults synchronize];
        
        LoginViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];

        [self presentViewController:loginView animated:YES completion:nil];
    }
}

- (void)postEditProfile
{
    UIImage *image = [UIImage imageWithData:UIImagePNGRepresentation(profileImage.image)];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    dataImage = [@"data:image/jpeg;base64," stringByAppendingString:[imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]];
    
    nameImage = [NSString stringWithFormat:@"%@.jpg",self.usernameTxt.text];
    
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 1"])
    {
        gradeInt = @"1";
    }
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 2"])
    {
        gradeInt = @"2";
    }
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 3"])
    {
        gradeInt = @"3";
    }
    
    NSDictionary *datauser = @{ @"uid": edUID,
                                @"username": usernameTxt.text,
                                @"mail": emailTxt.text,
                                @"fullname": nameTxt.text,
                                @"class": gradeInt,
                                @"interest": selectedSub.text,
                                @"prefix": prefixTxt.text,
                                @"province": provinceTxt.text,
                                @"birthday": bdTxt.text,
                                @"school": schoolTxt.text,
                                @"nameImage": nameImage,
                                @"image": dataImage };
    
    NSDictionary *result = [RESTAPI postEditPro:datauser];
    NSLog(@"%@",result);
}



/////***SELECTEDSUB***/////
- (IBAction)selectSubject:(id)sender
{
    switch ([sender tag])
    {
        case 1:
            if ([selectSub1 isSelected]==YES)
            {
                [selectSub1 setSelected:NO];
                
            } else {
                
                [selectSub1 setSelected:YES];
                //[selectSub2 setSelected:NO];
                
                [selectSub1 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub1 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub1 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 2:
            if ([selectSub2 isSelected]==YES)
            {
                [selectSub2 setSelected:NO];
                
            } else {
                
                [selectSub2 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub2 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub2 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub2 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 3:
            if ([selectSub3 isSelected]==YES)
            {
                [selectSub3 setSelected:NO];
                
            } else {
                
                [selectSub3 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub3 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub3 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub3 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 4:
            if ([selectSub4 isSelected]==YES)
            {
                [selectSub4 setSelected:NO];
                
            } else {
                
                [selectSub4 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub4 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub4 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub4 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 5:
            if ([selectSub5 isSelected]==YES)
            {
                [selectSub5 setSelected:NO];
                
            } else {
                
                [selectSub5 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub5 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub5 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub5 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 6:
            if ([selectSub6 isSelected]==YES)
            {
                [selectSub6 setSelected:NO];
                
            } else {
                
                [selectSub6 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub6 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub6 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub6 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 7:
            if ([selectSub7 isSelected]==YES)
            {
                [selectSub7 setSelected:NO];
                
            } else {
                
                [selectSub7 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub7 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub7 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub7 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 8:
            if ([selectSub8 isSelected]==YES)
            {
                [selectSub8 setSelected:NO];
                
            } else {
                
                [selectSub8 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub8 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub8 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub8 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 9:
            if ([selectSub9 isSelected]==YES)
            {
                [selectSub9 setSelected:NO];
                
            } else {
                
                [selectSub9 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub9 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub9 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub9 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        default:
            break;
    }
}
/////***SELECTEDSUB***/////


- (IBAction)editProfileComplete:(id)sender
{
    [self postEditProfile];
}

- (IBAction)logout:(id)sender
{
    [self postLogout];
}

- (IBAction)textFieldDismiss:(id)sender
{
    [usernameTxt resignFirstResponder];
    [prefixTxt resignFirstResponder];
    [nameTxt resignFirstResponder];
    [bdTxt resignFirstResponder];
    [schoolTxt resignFirstResponder];
    [gradeTxt resignFirstResponder];
    [provinceTxt resignFirstResponder];
    [emailTxt resignFirstResponder];
}


-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}


- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}



@end
