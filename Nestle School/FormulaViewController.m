//
//  FormulaViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "FormulaViewController.h"
#import "RESTAPI.h"

#import "DetailFormulaViewController.h"

@interface FormulaViewController ()
@end

@implementation FormulaViewController

@synthesize formulalbl;

@synthesize tidFormula;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    formulalbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    formulalbl.layer.shadowOffset = CGSizeMake(0, 1);
    formulalbl.layer.shadowOpacity = 1.0;
    formulalbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    [self getFormula];
}

- (void)getFormula
{
    NSString *urlString = [NSString stringWithFormat:@"service/list-formula/%@",tidFormula];
    NSLog(@"%@",urlString);
    
    formulaArray = [RESTAPI syncGET:urlString];
    //NSLog(@"%@", [classRoomArray description]);
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return formulaArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"app_19 copy"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"app_19 copy"];
    } else {
        background = [UIImage imageNamed:@"app_19 copy"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"formulaCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    formulaDic = [formulaArray objectAtIndex:indexPath.row];
    
    UILabel *titletxt = (UILabel *)[cell viewWithTag:101];
    titletxt.text = [formulaDic objectForKey:@"title"];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailFormulaViewController *detailFormulaView = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailFormulaViewController"];
    
    formulaDic = [formulaArray objectAtIndex:indexPath.row];
    
    detailFormulaView.nid = [formulaDic objectForKey:@"nid"];
    
    [self.navigationController pushViewController:detailFormulaView animated:YES];
}



- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
