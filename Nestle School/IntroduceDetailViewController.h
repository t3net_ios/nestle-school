//
//  CounselingRoomDetailViewController.h
//  Nestle School
//
//  Created by MOREMO on 10/25/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroduceDetailViewController : UIViewController <UIScrollViewDelegate>
{
    NSDictionary *detailIntroDic;
    
    NSArray *relatedArray;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollPageView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollTagView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageTagControl;

@property (weak, nonatomic) IBOutlet UILabel *counselRoomlbl;

@property (nonatomic, strong) NSString *nid;

@property (weak, nonatomic) IBOutlet UITextView *introTitle;
@property (weak, nonatomic) IBOutlet UITextView *introBody;

- (IBAction)back:(id)sender;

@end
