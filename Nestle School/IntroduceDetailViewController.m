//
//  CounselingRoomDetailViewController.m
//  Nestle School
//
//  Created by MOREMO on 10/25/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "IntroduceDetailViewController.h"
#import "RESTAPI.h"

#import "ClassSubLessonViewController.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface IntroduceDetailViewController ()
@end

@implementation IntroduceDetailViewController

@synthesize counselRoomlbl;
@synthesize nid;

@synthesize introTitle, introBody;

@synthesize scrollView, scrollPageView, scrollTagView;
@synthesize pageControl, pageTagControl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    counselRoomlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    counselRoomlbl.layer.shadowOffset = CGSizeMake(0, 1);
    counselRoomlbl.layer.shadowOpacity = 1.0;
    counselRoomlbl.layer.shadowRadius = 1.0;
    
    scrollPageView.delegate = self;
    scrollTagView.delegate = self;
    
    //////////////////////////////////////////////////
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = Color;
    
    [self getintroduceDetail];
}

- (void)getintroduceDetail
{
    NSString *urlString = [NSString stringWithFormat:@"service/tutor/%@",nid];
    NSLog(@"%@",urlString);
    
    detailIntroDic = (NSDictionary*)[RESTAPI syncGET:urlString];
    
    introTitle.text = [detailIntroDic objectForKey:@"tutor_title"];
    introTitle.font = [UIFont fontWithName:@"DBSaiKrokX" size:30.0f];
    [introTitle sizeToFit];
    
    NSArray *field_images = [detailIntroDic objectForKey:@"field_images"];
    
    for(int i = 0; i < [field_images count]; i++)
    {
        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                  [NSURL URLWithString:[field_images objectAtIndex:i]]]];
        
        UIImageView *introImage = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.scrollPageView.frame.size.width,
                                                                                0, self.scrollPageView.frame.size.width,
                                                                                self.scrollPageView.frame.size.height)];
        
        [introImage setImage:image];
        [introImage setContentMode:UIViewContentModeScaleAspectFit];
        [self.scrollPageView addSubview:introImage];
    }
    
    
    relatedArray = [detailIntroDic objectForKey:@"related_playlists"];
    
    int i = 0;
    for (NSDictionary *relatedDic in relatedArray)
    {
        NSString *dic_image = [relatedDic objectForKey:@"playlist_image"];
        
        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                  [NSURL URLWithString:dic_image]]];
        
        UIButton * tagBanner = [[UIButton alloc] initWithFrame:CGRectMake(i*self.scrollTagView.frame.size.width/2,
                                                                          0, self.scrollTagView.frame.size.width/2,
                                                                          self.scrollTagView.frame.size.height)];
        
        
        tagBanner.tag = i ;
        
        [tagBanner addTarget:self
                      action:@selector(methodTouchUpInside:)
            forControlEvents:UIControlEventTouchUpInside];
        
        [tagBanner setImage:image forState:UIControlStateNormal];
        [self.scrollTagView addSubview:tagBanner];
        
        i++;
    }

    self.scrollPageView.contentSize = CGSizeMake(self.scrollPageView.frame.size.width * [field_images count],
                                                 self.scrollPageView.frame.size.height-100);

    self.scrollTagView.contentSize = CGSizeMake(self.scrollTagView.frame.size.width/2 * [relatedArray count],
                                                self.scrollTagView.frame.size.height-100);
    
    pageControl.currentPage = 0;
    pageControl.numberOfPages = [field_images count];
    scrollPageView.pagingEnabled = YES;
    
    pageTagControl.currentPage = 0;
    pageTagControl.numberOfPages = [relatedArray count]/2;
    scrollTagView.pagingEnabled = YES;

    
    NSString *body = [detailIntroDic objectForKey:@"tutor_description"];
    NSAttributedString *bodyhtml = [[NSAttributedString alloc]
                                    initWithData:[body dataUsingEncoding:NSUnicodeStringEncoding]
                                    options:@{ NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType }
                                    documentAttributes:nil
                                    error:nil];
    
    NSString *bodyString = [bodyhtml string];
    bodyString = [bodyString stringByReplacingOccurrencesOfString:@"<p>" withString:@" "];
    bodyString = [bodyString stringByReplacingOccurrencesOfString:@"</p>" withString:@" "];
    
    introBody.text = bodyString;
    introBody.font = [UIFont fontWithName:@"DBSaiKrokX" size:25.0f];
    [introBody sizeToFit];
    
    
    float introBodyHeight = introBody.frame.size.height;
    float imageHeight = self.scrollPageView.frame.size.height;
    float vdotagHeight = self.scrollTagView.frame.size.height;
    float footerOffset = 150;
    //NSLog(@"formulaBodyHeight %f",formulaBodyHeight);
    [introBody setScrollEnabled:NO];
    
    [self.pageTagControl setCenter:CGPointMake(self.view.frame.size.width/2,
                                            introBodyHeight + imageHeight + 70)];

    [self.scrollTagView setCenter:CGPointMake(self.view.frame.size.width/2,
                                              introBodyHeight + imageHeight + vdotagHeight)];
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,
                                          introBodyHeight + imageHeight + vdotagHeight + footerOffset)];

}

- (void)methodTouchUpInside:(UIButton*)sender
{
    NSLog(@"TouchUpInside sender %ld",(long)sender.tag);
    
    ClassSubLessonViewController *classSubLessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassSubLessonViewController"];
    
    NSDictionary *groupDic = [relatedArray objectAtIndex:sender.tag];
    
    classSubLessonView.tid = [groupDic objectForKey:@"playlist_id"];
    classSubLessonView.name = [groupDic objectForKey:@"playlist_title"];
    classSubLessonView.pdf = [groupDic objectForKey:@"playlist_pdf"];
    
    [self.navigationController pushViewController:classSubLessonView animated:YES];
}


- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)scrollViewDidScroll:(UIScrollView *)localScrollView
{
    pageControl.currentPage = localScrollView.contentOffset.x/self.view.frame.size.width;
    
    pageTagControl.currentPage = localScrollView.contentOffset.x/self.view.frame.size.width;
}



@end
