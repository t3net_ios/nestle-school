//
//  CounselingRoomViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroduceViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *introduceArray;
    NSDictionary *introduceDic;
    
    NSString *nid;
    
    NSString *title;
    NSString *field_images;
}

@property (weak, nonatomic) IBOutlet UILabel *counselRoomlbl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


