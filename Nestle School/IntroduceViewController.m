//
//  CounselingRoomViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "IntroduceViewController.h"
#import "RESTAPI.h"

#import "IntroduceDetailViewController.h"

@interface IntroduceViewController ()
@end

@implementation IntroduceViewController

@synthesize counselRoomlbl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    counselRoomlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    counselRoomlbl.layer.shadowOffset = CGSizeMake(0, 1);
    counselRoomlbl.layer.shadowOpacity = 1.0;
    counselRoomlbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    [self getIntroduce];
}

-(void)getIntroduce
{
    NSString *urlString = [NSString stringWithFormat:@"service/list-tutor"];
    NSLog(@"%@",urlString);
    
    introduceArray = [RESTAPI syncGET:urlString];
    
    for (NSDictionary *getIntroduceDict in introduceArray)
    {
        NSString *dic_nid = [getIntroduceDict objectForKey:@"nid"];
        NSString *dic_field_images = [getIntroduceDict objectForKey:@"field_images"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL  *url = [NSURL URLWithString:dic_field_images];
            NSData *urlData = [NSData dataWithContentsOfURL:url];
            if ( urlData )
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                
                NSString *filenameOnly = [dic_field_images lastPathComponent];
                NSString *filePath = [NSString stringWithFormat:@"%@/%@_%@", documentsDirectory,dic_nid,filenameOnly];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [urlData writeToFile:filePath atomically:YES];
                    [_tableView reloadData];
                });
            }
        });
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return introduceArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"BG4"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"BG4"];
    } else {
        background = [UIImage imageNamed:@"BG4"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"counselingroomCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    introduceDic = [introduceArray objectAtIndex:indexPath.row];
    
    UIImageView *tutorImageView = (UIImageView *)[cell viewWithTag:102];
    UILabel *title_tag = (UILabel *)[cell viewWithTag:101];
    UILabel *nid_tag = (UILabel *)[cell viewWithTag:103];
    
    title_tag.text = [introduceDic objectForKey:@"title"];
    nid_tag.text = [introduceDic objectForKey:@"nid"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filenameOnly = [introduceDic objectForKey:@"field_images"];
    NSString *dic_nid = [introduceDic objectForKey:@"nid"];
    filenameOnly = [filenameOnly lastPathComponent];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@_%@", documentsDirectory,dic_nid,filenameOnly];
    
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    tutorImageView.image = image;
    tutorImageView.contentMode = UIViewContentModeScaleAspectFill;
    tutorImageView.layer.cornerRadius = tutorImageView.frame.size.width / 2;
    tutorImageView.layer.borderWidth = 3.0f;
    tutorImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    tutorImageView.clipsToBounds = YES;
    tutorImageView.image = image;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IntroduceDetailViewController *introduceDetailView = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroduceDetailViewController"];
    
    introduceDic = [introduceArray objectAtIndex:indexPath.row];
    
    introduceDetailView.nid = [introduceDic objectForKey:@"nid"];

    [self.navigationController pushViewController:introduceDetailView animated:YES];
}



@end
