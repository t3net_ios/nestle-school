//
//  LoginViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/21/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UIAlertViewDelegate>
{
    NSArray *arrayResult;
    
    NSDictionary *profileData;
}
@property (weak, nonatomic) IBOutlet UILabel *loginlbl;

@property (weak, nonatomic) IBOutlet UITextField *usernameTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;

@property (nonatomic, strong) NSString *nameFB;
@property (nonatomic, strong) NSString *emailFB;
@property (nonatomic, strong) NSString *urlPhotoFB;
@property (nonatomic, strong) NSString *idFB;
@property (nonatomic, strong) NSString *tkFB;
@property (nonatomic, strong) NSString *fbAccessToken;

- (IBAction)loginbtn:(id)sender;

- (IBAction)signupWithFacebook:(id)sender;

- (IBAction)registerNew:(id)sender;

- (IBAction)forgotPass:(id)sender;

- (IBAction)textFieldDismiss:(id)sender;

@end
