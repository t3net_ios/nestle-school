//
//  LoginViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/21/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "LoginViewController.h"

#import "RESTAPI.h"
#import "SCLAlertView.h"

#import "RegisterViewController.h"
#import "ProfileViewController.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "SCLAlertView.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface LoginViewController ()
@end

@implementation LoginViewController

@synthesize loginlbl;

@synthesize usernameTxt;
@synthesize passwordTxt;

@synthesize nameFB;
@synthesize emailFB;
@synthesize urlPhotoFB;
@synthesize idFB;
@synthesize tkFB;

@synthesize fbAccessToken;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    loginlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    loginlbl.layer.shadowOffset = CGSizeMake(0,1);
    loginlbl.layer.shadowOpacity = 1.0;
    loginlbl.layer.shadowRadius = 1.0;
    
    profileData = [[NSUserDefaults standardUserDefaults] valueForKey:@"profile"];
}

- (void)signupWithFacebook
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logInWithReadPermissions:@[@"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     
     {
         if (error)
         {
             NSLog(@"Process error");
             
         } else if (result.isCancelled) {
             
             NSLog(@"Cancelled");
             
         } else {
             
             NSLog(@"Logged in");
             
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 //NSLog(@"result is : %@",result);
                 [self fetchUserInfo];
                 
             }
         }
     }];
}

-(void)fetchUserInfo
{

    if ([FBSDKAccessToken currentAccessToken])
    {
        fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
        //NSLog(@"FBAccessToken : %@",fbAccessToken);

        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                           parameters:@{ @"fields": @"id, name, first_name, last_name, picture.width(300).height(300), email, birthday, location" }]
         
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 NSDictionary *datauser = @{ @"nestleLF": @"loginFacebook",
                                             @"facebook_id": result[@"id"] };

                 NSDictionary *resultFB = [RESTAPI postLoginFB:datauser];

                 NSLog(@"%@", resultFB);
                 
                 if([resultFB[@"status"] isEqualToString:@"success"])
                 {
                     arrayResult = resultFB[@"uid"];
                     
                     if (arrayResult != nil)
                     {
                         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                         [defaults setBool:YES forKey:@"registered"];
                         [defaults synchronize];

                         if ([[NSUserDefaults standardUserDefaults] boolForKey:@"registered"])
                         {
                             [self alertComplete];
                         }
                     }
                     
                 } else {
                     
                     NSString *imageStringOfLoginUser = [[[resultFB valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                     
                     nameFB = resultFB[@"first_name"];
                     emailFB = resultFB[@"email"];
                     urlPhotoFB = imageStringOfLoginUser;
                     idFB = [resultFB objectForKey:@"id"];
                     tkFB = fbAccessToken;
                     
                     
                     RegisterViewController *registerView = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
                     
                     registerView.nameFB = self.nameFB;
                     registerView.emailFB = self.emailFB;
                     registerView.urlPhotoRG = self.urlPhotoFB;
                     registerView.idFB = self.idFB;
                     registerView.tkFB = self.fbAccessToken;
                     
                     [self presentViewController:registerView animated:YES completion:nil];
                 }
             
             } else {
                 
                 NSLog(@"Error : %@",error);
                 
             }
         }];
    }
}

- (IBAction)loginbtn:(id)sender
{
    if (self.usernameTxt.text.length == 0 || self.passwordTxt.text == 0)
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
        [alert setBodyTextFontFamily:@"DBSaiKrokX" withSize:25.0f];
        [alert setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
        
        [alert showCustom:self
                    image:[UIImage imageNamed:@"app_01"]
                    color:Color
                    title:@"Login ไม่สำเร็จ"
                 subTitle:@"กรุณากรอก Username และ Password ให้ถูกต้อง"
         closeButtonTitle:@"OK"
                 duration:0.0f];
        
    } else {
        
        NSDictionary *datauser = @{ @"user": self.usernameTxt.text,
                                    @"pass": self.passwordTxt.text };

        NSDictionary *result = [RESTAPI postLogin:datauser];
        
        if (result)
        {
            arrayResult = [result objectForKey:@"uid"];

            if (arrayResult != nil)
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setBool:YES forKey:@"registered"];
                [defaults synchronize];

                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"registered"])
                {
                    
                    [self alertComplete];
                    
                }
            
            } else {
                
                SCLAlertView *alert = [[SCLAlertView alloc] init];
                
                [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
                [alert setBodyTextFontFamily:@"DBSaiKrokX" withSize:25.0f];
                [alert setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
                
                [alert showCustom:self
                            image:[UIImage imageNamed:@"app_01"]
                            color:Color
                            title:@"Login ไม่สำเร็จ"
                         subTitle:@"กรุณากรอก Username และ Password ใหม่อีกครั้ง"
                 closeButtonTitle:@"OK"
                         duration:0.0f];

            }
        }
    }
}

- (void)forgetPassword
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
    [alert setBodyTextFontFamily:@"DBSaiKrokX" withSize:20.0f];
    [alert setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
    
    SCLTextView *textField = [alert addTextField:@"E - mail"];
    
    [alert addButton:@"ดำเนินการต่อ" actionBlock:^(void) {
        
        NSDictionary *forgotPassDic = @{ @"action" : @"forgetPassword",
                                         @"mail" : textField.text };
        
        NSDictionary *forgotPass = [RESTAPI syncPOST:@"service/forget-password" parameters:forgotPassDic];
        
     
        if ([forgotPass[@"status"] isEqualToString:@"success"])
        {
            NSString *uid = forgotPass[@"uid"];
            
            SCLAlertView *alert2 = [[SCLAlertView alloc] init];
            
            [alert2 setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
            [alert2 setBodyTextFontFamily:@"DBSaiKrokX" withSize:20.0f];
            [alert2 setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
            
            SCLTextView *textField = [alert2 addTextField:@"รหัสผ่าน"];
            alert2.hideAnimationType = SCLAlertViewHideAnimationSimplyDisappear;
            
            [alert2 addButton:@"ดำเนินการต่อ" actionBlock:^(void) {
                
                NSLog(@"%@", forgotPass[@"uid"]);
                
                NSDictionary *updatePassDic = @{ @"uid" : uid,
                                                 @"newPass" : textField.text };
                
                NSDictionary *updatePass = [RESTAPI syncPOST:@"service/update-password" parameters:updatePassDic];
                
                if ([updatePass[@"status"] isEqualToString:@"success"])
                {
                    SCLAlertView *alert3 = [[SCLAlertView alloc] init];
                    
                    [alert3 setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
                    [alert3 setBodyTextFontFamily:@"DBSaiKrokX" withSize:20.0f];
                    [alert3 setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
                    
                    [alert3 showCustom:self
                                 image:[UIImage imageNamed:@"app_01"]
                                 color:Color
                                 title:@"ตั้งรหัสผ่านใหม่"
                              subTitle:@"การตั้งค่าใหม่สำเร็จแล้ว"
                      closeButtonTitle:@"เข้าสู่ระบบ"
                              duration:0.0f];
                }

            }];
            
            [alert2 showCustom:self
                         image:[UIImage imageNamed:@"app_01"]
                         color:Color
                         title:@"ตั้งรหัสผ่านใหม่"
                      subTitle:@""
              closeButtonTitle:nil
                      duration:0.0f];
        }
    
    }];
    
    [alert showCustom:self
                image:[UIImage imageNamed:@"app_01"]
                color:Color
                title:@"กรุณาระบุ E-mail ที่ลงทะเบียนไว้"
             subTitle:@""
     closeButtonTitle:nil
             duration:0.0f];

}

- (void)alertComplete
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *tabcontroller = [mainStoryboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    
    tabcontroller.selectedIndex = 2;
    
    UITabBar *tabBar = tabcontroller.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    
    tabBarItem1.selectedImage = [[UIImage imageNamed:@"ico_1_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem1.image = [[UIImage imageNamed:@"ico_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem2.selectedImage = [[UIImage imageNamed:@"ico_2_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem2.image = [[UIImage imageNamed:@"ico_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem3.selectedImage = [[UIImage imageNamed:@"BUTTON3-2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem3.image = [[UIImage imageNamed:@"BUTTON3.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem4.selectedImage = [[UIImage imageNamed:@"ico_3_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem4.image = [[UIImage imageNamed:@"ico_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem5.selectedImage = [[UIImage imageNamed:@"ico_4_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem5.image = [[UIImage imageNamed:@"ico_4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self presentViewController:tabcontroller animated:YES completion:nil];
}



- (IBAction)signupWithFacebook:(id)sender
{
    [self signupWithFacebook];
}

- (IBAction)registerNew:(id)sender
{
    UIViewController *storyboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                    instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    
    [self presentViewController:storyboard animated:YES completion:nil];
}

- (IBAction)forgotPass:(id)sender
{
    [self forgetPassword];
}

- (IBAction)textFieldDismiss:(id)sender
{
    [usernameTxt resignFirstResponder];
    [passwordTxt resignFirstResponder];
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}



@end
