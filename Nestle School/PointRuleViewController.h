//
//  PointRuleViewController.h
//  Nestle School
//
//  Created by MOREMO on 11/7/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PointRuleViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pointrulelbl;

- (IBAction)back:(id)sender;

@end
