//
//  PointRuleViewController.m
//  Nestle School
//
//  Created by MOREMO on 11/7/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "PointRuleViewController.h"

@interface PointRuleViewController ()
@end

@implementation PointRuleViewController

@synthesize pointrulelbl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    pointrulelbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    pointrulelbl.layer.shadowOffset = CGSizeMake(0, 1);
    pointrulelbl.layer.shadowOpacity = 1.0;
    pointrulelbl.layer.shadowRadius = 1.0;
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:^{
        //
    }];
    
    [self.navigationController popViewControllerAnimated:NO];
}


@end
