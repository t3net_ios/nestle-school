//
//  ProfilePointViewController.h
//  Nestle School
//
//  Created by MOREMO on 11/3/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilePointViewController : UIViewController
{
    NSInteger topOffset;
    NSInteger footerOffset;
    NSInteger leftOffset;
}

@property (weak, nonatomic) IBOutlet UIScrollView *pointScrollView;
@property (weak, nonatomic) IBOutlet UILabel *profilePointlbl;
@property (weak, nonatomic) IBOutlet UIImageView *profilePointImage;

@property (weak, nonatomic) IBOutlet UILabel *profilePointName;
@property (weak, nonatomic) IBOutlet UILabel *profilePointLevel;
@property (weak, nonatomic) IBOutlet UILabel *profilePointXP;
@property (weak, nonatomic) IBOutlet UILabel *profilePointPoint;
@property (weak, nonatomic) IBOutlet UIProgressView *profileXPbar;

@property (nonatomic, strong) NSString *ppUID;
@property (nonatomic, strong) NSString *ppImage;
@property (nonatomic, strong) NSString *ppName;
@property (nonatomic, strong) NSString *ppLevel;
@property (nonatomic, strong) NSString *ppPoint;
@property (nonatomic, strong) NSString *ppXPBar;
@property (nonatomic, strong) NSString *ppXPString;

- (IBAction)pointrulebtn:(id)sender;
- (IBAction)back:(id)sender;

@end


/*
 
 
 //    float s2 = [[historyScores[2] objectForKey:@"field_score"] floatValue] / [[historyScores[2] objectForKey:@"field_rule_number"] floatValue];
 //    float s1 = [[historyScores[1] objectForKey:@"field_score"] floatValue] / [[historyScores[1] objectForKey:@"field_rule_number"] floatValue];
 //    float s0 = [[historyScores[0] objectForKey:@"field_score"] floatValue] / [[historyScores[0] objectForKey:@"field_rule_number"] floatValue];
 //
 //    float scorestrAll = s0 + s1 + s2;
 //
 //    float percent = (scorestrAll * 100)/[[historyScores[0] objectForKey:@"field_rule_number"] floatValue];
 //    NSInteger percents = (int)roundf(percent);
 //
 //    totalper.text = [NSString stringWithFormat:@"%ld", (long)percents];
 */
