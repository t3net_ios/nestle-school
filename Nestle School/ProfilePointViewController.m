//
//  ProfilePointViewController.m
//  Nestle School
//
//  Created by MOREMO on 11/3/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ProfilePointViewController.h"
#import "RESTAPI.h"

#import "PointRuleViewController.h"

#define IS_IPHONE_4S (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON)

@interface ProfilePointViewController ()
@end

@implementation ProfilePointViewController

@synthesize profilePointlbl;
@synthesize profilePointImage, profilePointName, profilePointLevel, profilePointPoint, profileXPbar;
@synthesize ppUID, ppImage, ppName, ppLevel, ppPoint, ppXPString;

@synthesize pointScrollView;

- (void)viewDidLoad
{
    [super viewDidLoad];

    profilePointlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    profilePointlbl.layer.shadowOffset = CGSizeMake(0, 1);
    profilePointlbl.layer.shadowOpacity = 1.0;
    profilePointlbl.layer.shadowRadius = 1.0;
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568)
    {
        topOffset = 370;
        footerOffset = 600;
        leftOffset = 10;
    }
    
    else if (screenBounds.size.height == 667)
    {
        topOffset = 470;
        footerOffset = 700;
        leftOffset = 15;
    }
    
    else if (screenBounds.size.height == 736)
    {
        topOffset = 540;
        footerOffset = 770;
        leftOffset = 30;
    }
    
    
    
    //////////////////////////////////////////////////
    NSString *urlPicture = ppImage;
    NSURL *url = [NSURL URLWithString:urlPicture];
    NSData *dataImage = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:dataImage];
    
    self.profilePointImage.layer.cornerRadius = self.profilePointImage.frame.size.width / 2;
    self.profilePointImage.layer.borderWidth = 3.0f;
    self.profilePointImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profilePointImage.clipsToBounds = YES;
    
    [self.profilePointImage setImage:image];

    profilePointName.text = ppName;
    profilePointLevel.text = ppLevel;
    profilePointPoint.text = ppPoint;
    
    [profileXPbar setTransform:CGAffineTransformMakeScale(1.0, 7.0)];
    
    [profileXPbar setProgressImage:[[UIImage imageNamed:@"app_83"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 100, 100)]];//เทา
    
    profileXPbar.layer.cornerRadius = 10.0f;
    profileXPbar.clipsToBounds = YES;
    
    float xp = [self.ppXPBar  floatValue] / 100;
    self.profileXPbar.progress = xp;
    self.profilePointXP.text = ppXPString;
    
    [self getStamp];
}

- (void)getStamp
{
    NSString *urlStampString = [NSString stringWithFormat:@"service/stamp"];
    NSLog(@"%@",urlStampString);
    NSArray *stamps = [RESTAPI syncGET:urlStampString];
    //NSLog(@"%@",stamps);
    
    NSString *urlSpString = [NSString stringWithFormat:@"service/special-stamp"];
    NSLog(@"%@",urlSpString);
    NSArray *specialstamps = [RESTAPI syncGET:urlSpString];
    //NSLog(@"%@",specialstamps);
    
    NSString *urlHistorytring = [NSString stringWithFormat:@"service/history-special-stamp/%@",ppUID];
    NSLog(@"%@",urlHistorytring);
    NSArray *historys = [RESTAPI syncGET:urlHistorytring];
    //NSLog(@"%@",historys);
    
    ////////////////////////////////////////
    NSInteger i = 0;
    NSInteger row = 0 ;
    NSInteger column = 0 ;
    NSInteger maxPerRow = 5 ;
    NSInteger stampWidth = 70 ;
    NSInteger stampHeight = 70 ;
    NSInteger totalStampHeight = stampHeight ;
    
    for(NSDictionary *stamp in stamps)
    {
        row = (i/maxPerRow);
        if(column == maxPerRow)
        {
            column = 0;
        }
        
        totalStampHeight = row*stampHeight;
        
        UIButton *stampButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //stampButton.contentMode = UIViewContentModeScaleAspectFit;
        
        [stampButton setFrame:CGRectMake(leftOffset+(column*stampWidth), topOffset+(totalStampHeight), stampWidth, stampHeight)];
        
        [stampButton setTitle:[NSString stringWithFormat:@"%@",[stamp objectForKey:@"field_xp"]] forState:UIControlStateNormal];
        //[stampButton setTitle:[NSString stringWithFormat:@"%ld",(long)i] forState:UIControlStateNormal];
        
        if([ppPoint integerValue] > [[stamp valueForKey:@"field_xp"] integerValue])
        {
            NSString *urlStampImage = [stamp objectForKey:@"field_image"];
            NSURL *url = [NSURL URLWithString:urlStampImage];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *stampImage = [[UIImage alloc] initWithData:data];
            
            [stampButton setImage:stampImage forState:UIControlStateNormal];
            
        }else{
            //Get Stamp image
            [stampButton setImage:[UIImage imageNamed:@"app_95"] forState:UIControlStateNormal];
        }
        
        [pointScrollView addSubview:stampButton];
        [pointScrollView setContentSize:CGSizeMake(pointScrollView.frame.size.width, totalStampHeight + footerOffset)];
        
        i++;
        column++;
    }
    
    ////////////////////////////////////////
    NSInteger spfooterOffset = footerOffset+150;
    NSInteger spi = 0;
    NSInteger sprow = 0 ;
    NSInteger spcolumn = 0 ;
    NSInteger spmaxPerRow = 5 ;
    NSInteger spstampWidth = 70 ;
    NSInteger spstampHeight = 70 ;
    NSInteger sptotalStampHeight = spstampHeight ;
    NSInteger sptopOffset = topOffset+totalStampHeight+150;
    
    for(NSDictionary *specialstamp in specialstamps)
    {
        sprow = (spi/spmaxPerRow);
        if(spcolumn == spmaxPerRow)
        {
            spcolumn = 0;
        }
        
        sptotalStampHeight = sprow*spstampHeight;
        
        UIButton *spstampButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [spstampButton setFrame:CGRectMake(leftOffset+(spcolumn*spstampWidth), sptopOffset+(sptotalStampHeight), spstampWidth, spstampHeight)];
    
        [spstampButton setImage:[UIImage imageNamed:@"app_95"] forState:UIControlStateNormal];
        
        [pointScrollView addSubview:spstampButton];
        [pointScrollView setContentSize:CGSizeMake(pointScrollView.frame.size.width, sptotalStampHeight + spfooterOffset + 400)];
        
        spi++;
        spcolumn++;
    }
    
    
    ////////////////////////////////////////
    NSInteger lvi = 0;
    NSInteger lvrow = 0 ;
    NSInteger lvcolumn = 0 ;
    NSInteger lvmaxPerRow = 5 ;
    NSInteger lvstampWidth = 70 ;
    NSInteger lvstampHeight = 70 ;
    NSInteger lvtotalStampHeight = lvstampHeight ;
    NSInteger lvtopOffset = topOffset+totalStampHeight+150;

    for(NSDictionary *history in historys)
    {
        lvrow = (lvi/lvmaxPerRow);
        if(lvcolumn == lvmaxPerRow)
        {
            lvcolumn = 0;
        }

        lvtotalStampHeight = lvrow*lvstampHeight;
        
        UIButton *lvstampButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [lvstampButton setFrame:CGRectMake(leftOffset+(lvcolumn*lvstampWidth), lvtopOffset+(lvtotalStampHeight), lvstampWidth, lvstampHeight)];
        
        NSString *urlStampImage = [history objectForKey:@"field_image"];
        NSURL *url = [NSURL URLWithString:urlStampImage];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *stampImage = [[UIImage alloc] initWithData:data];
        
        [lvstampButton setImage:stampImage forState:UIControlStateNormal];
        
        [pointScrollView addSubview:lvstampButton];
        [pointScrollView setContentSize:CGSizeMake(pointScrollView.frame.size.width, sptotalStampHeight + spfooterOffset + 400)];
        
        lvi++;
        lvcolumn++;
    }

}


- (IBAction)pointrulebtn:(id)sender
{
    PointRuleViewController *pointRuleView = [self.storyboard instantiateViewControllerWithIdentifier:@"PointRuleViewController"];
    
    [self.navigationController pushViewController:pointRuleView animated:YES];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
