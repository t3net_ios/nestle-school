//
//  ProfileScoreViewController.h
//  Nestle School
//
//  Created by MOREMO on 11/3/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileScoreViewController : UIViewController
{
//    NSMutableArray *profilescoreArray;
    NSArray *profilescoreArray;
    
    NSMutableArray *scoresArray;
    NSDictionary *dictionary;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *profileScorelbl;
@property (weak, nonatomic) IBOutlet UIImageView *profileScoreImage;

@property (weak, nonatomic) IBOutlet UILabel *profileScoreName;
@property (weak, nonatomic) IBOutlet UILabel *profileScoreLevel;
@property (weak, nonatomic) IBOutlet UILabel *profileScoreXP;
@property (weak, nonatomic) IBOutlet UILabel *profileScorePoint;
@property (weak, nonatomic) IBOutlet UIProgressView *profileXPbar;

@property (nonatomic, strong) NSString *psUID;
@property (nonatomic, strong) NSString *psImage;
@property (nonatomic, strong) NSString *psName;
@property (nonatomic, strong) NSString *psLevel;
@property (nonatomic, strong) NSString *psPoint;
@property (nonatomic, strong) NSString *psXPBar;
@property (nonatomic, strong) NSString *psXPString;

- (IBAction)back:(id)sender;
- (IBAction)viewPoint:(id)sender;

@end
