//
//  ProfileScoreViewController.m
//  Nestle School
//
//  Created by MOREMO on 11/3/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ProfileScoreViewController.h"

#import "ProfilePointViewController.h"

#import "RESTAPI.h"

@interface ProfileScoreViewController ()
@end

@implementation ProfileScoreViewController

@synthesize profileScorelbl;

@synthesize profileScoreImage, profileScoreName, profileScoreLevel, profileScorePoint, profileXPbar;

@synthesize psUID, psImage, psName, psLevel, psPoint, psXPString;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    profileScorelbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    profileScorelbl.layer.shadowOffset = CGSizeMake(0, 1);
    profileScorelbl.layer.shadowOpacity = 1.0;
    profileScorelbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    NSString *urlPicture = psImage;
    NSURL *url = [NSURL URLWithString:urlPicture];
    NSData *dataImage = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:dataImage];
    
    self.profileScoreImage.layer.cornerRadius = self.profileScoreImage.frame.size.width / 2;
    self.profileScoreImage.layer.borderWidth = 3.0f;
    self.profileScoreImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileScoreImage.clipsToBounds = YES;
    
    [self.profileScoreImage setImage:image];
    
    profileScoreName.text = psName;
    profileScoreLevel.text = psLevel;
    profileScorePoint.text = psPoint;
    
    [profileXPbar setTransform:CGAffineTransformMakeScale(1.0, 7.0)];
    
    [profileXPbar setProgressImage:[[UIImage imageNamed:@"app_83"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 100, 0)]]; //เทา
    profileXPbar.layer.cornerRadius = 10.0f;
    profileXPbar.clipsToBounds = YES;
    
    float xp = [self.psXPBar  floatValue] / 100;
    self.profileXPbar.progress = xp;
    self.profileScoreXP.text = psXPString;

    [self getHistoryScore];
}

-(void)getHistoryScore
{
    NSString *urlString = [NSString stringWithFormat:@"service/user-performance/%@",psUID];
    NSLog(@"%@",urlString);
    
    profilescoreArray = [RESTAPI syncGET:urlString];
    
    if ([profilescoreArray count] == 0)
    {
        NSLog(@"NULL");
    } else {
        scoresArray = [NSMutableArray new];
        
        for (NSDictionary *dataDict in profilescoreArray){
            
            NSString *str_scores = [dataDict valueForKey:@"scores"];
            
            NSArray *scores = (NSArray*)str_scores;
            [scoresArray addObject:scores];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return profilescoreArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"profilescoreCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    NSArray *userperformance = [profilescoreArray objectAtIndex:indexPath.row];
    NSArray *historyScores = [scoresArray objectAtIndex:indexPath.row];
    
    UILabel *title = (UILabel *)[cell viewWithTag:111];
    UILabel *totalper = (UILabel *)[cell viewWithTag:222];
    UIButton *testAgain = (UIButton *)[cell viewWithTag:333];
    //UIButton *shared = (UIButton *)[cell viewWithTag:444];

    title.text = [userperformance valueForKey:@"title"];
    totalper.text = [[userperformance valueForKey:@"performance_percent"] stringValue];
    
    [testAgain addTarget:self
                       action:@selector(taptestAgain:)
             forControlEvents:UIControlEventTouchUpInside];
    testAgain.tag = indexPath.row;
    
    UILabel *date0 = (UILabel *)[cell viewWithTag:101];
    UILabel *date1 = (UILabel *)[cell viewWithTag:102];
    UILabel *date2 = (UILabel *)[cell viewWithTag:103];
    
    UILabel *rule0 = (UILabel *)[cell viewWithTag:201];
    UILabel *rule1 = (UILabel *)[cell viewWithTag:202];
    UILabel *rule2 = (UILabel *)[cell viewWithTag:203];

    UIProgressView *score0 = (UIProgressView *)[cell viewWithTag:301];
    [score0 setTransform:CGAffineTransformMakeScale(1.0, 7.0)];
    score0.clipsToBounds = YES;
    UIProgressView *score1 = (UIProgressView *)[cell viewWithTag:302];
    [score1 setTransform:CGAffineTransformMakeScale(1.0, 7.0)];
    score1.clipsToBounds = YES;
    UIProgressView *score2 = (UIProgressView *)[cell viewWithTag:303];
    [score2 setTransform:CGAffineTransformMakeScale(1.0, 7.0)];
    score2.clipsToBounds = YES;
    
    [score0 setTrackImage:[[UIImage imageNamed:@"app_82"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    [score0 setProgressImage:[[UIImage imageNamed:@"app_83"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 100, 0)]];
    
    [score1 setTrackImage:[[UIImage imageNamed:@"app_82"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    [score1 setProgressImage:[[UIImage imageNamed:@"app_83"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 100, 0)]];
    
    [score2 setTrackImage:[[UIImage imageNamed:@"app_82"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    [score2 setProgressImage:[[UIImage imageNamed:@"app_83"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 100, 0)]];
    
    if(historyScores.count > 2){
        
        date2.text = [historyScores[2] objectForKey:@"exam_date"];
        rule2.text = [historyScores[2] objectForKey:@"exam_total_score"];
        
        float s2 = [[historyScores[2] objectForKey:@"exam_score"] floatValue] / [[historyScores[2] objectForKey:@"exam_total_score"] floatValue];
        [score2 setProgress:s2];
    }
    
    if(historyScores.count > 1){
        
        date1.text = [historyScores[1] objectForKey:@"exam_date"];
        rule1.text = [historyScores[1] objectForKey:@"exam_total_score"];
        
        float s1 = [[historyScores[1] objectForKey:@"exam_score"] floatValue] / [[historyScores[1] objectForKey:@"exam_total_score"] floatValue];
        [score1 setProgress:s1];
    }
    
    if(historyScores.count > 0){
    
        date0.text = [historyScores[0] objectForKey:@"exam_date"];
        rule0.text = [historyScores[0] objectForKey:@"exam_total_score"];
        
        float s0 = [[historyScores[0] objectForKey:@"exam_score"] floatValue] / [[historyScores[0] objectForKey:@"exam_total_score"] floatValue];
        [score0 setProgress:s0];

    }
    
    return cell;
}

- (void)taptestAgain:(UIButton*)sender
{
    UIButton *senderButton = (UIButton *)sender;
    NSInteger currentRow = senderButton.tag;
    NSLog(@"currentRow = %ld",(long)sender.tag);
    
    //NSString *nid = [profilescoreArray[currentRow] objectForKey:@"nid"];
}

- (IBAction)viewPoint:(id)sender
{
    ProfilePointViewController *profilePointView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilePointViewController"];
    
    [self presentViewController:profilePointView animated:YES completion:nil];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
