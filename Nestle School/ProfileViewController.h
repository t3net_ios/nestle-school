//
//  ProfileViewController.h
//  Nestle School
//
//  Created by MOREMO on 11/3/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NuweScoreCharts/NuweScoreCharts.h>

@interface ProfileViewController : UIViewController
{
    NSDictionary *profileData;
    
    NSString *profile_name;
    NSString *profile_percent;
    
    NSInteger score;
}
@property (strong, nonatomic) IBOutlet NUDialChart *BottomDialChart1;

@property (weak, nonatomic) IBOutlet UILabel *profilelbl;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *profileLevel;
@property (weak, nonatomic) IBOutlet UILabel *profileXP;
@property (weak, nonatomic) IBOutlet UILabel *profilePoint;
@property (weak, nonatomic) IBOutlet UIButton *profilePercent;
@property (weak, nonatomic) IBOutlet UIProgressView *profileXPbar;

@property (nonatomic, strong) NSString *xpBar;


- (IBAction)editProfile:(id)sender;

- (IBAction)viewPoint:(id)sender;
- (IBAction)viewScore:(id)sender;

@end
