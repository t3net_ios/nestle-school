//
//  ProfileViewController.m
//  Nestle School
//
//  Created by MOREMO on 11/3/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ProfileViewController.h"
#import "RESTAPI.h"

#import "ProfilePointViewController.h"
#import "ProfileScoreViewController.h"
#import "EditProfileViewController.h"

@interface ProfileViewController ()
@end

@implementation ProfileViewController

@synthesize profilelbl;

@synthesize profileImage, profileName, profileLevel, profilePoint, profilePercent, profileXP, profileXPbar;
@synthesize BottomDialChart1;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    profilelbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    profilelbl.layer.shadowOffset = CGSizeMake(0, 1);
    profilelbl.layer.shadowOpacity = 1.0;
    profilelbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    profileData = [[NSUserDefaults standardUserDefaults] valueForKey:@"profile"];
    //NSLog(@"data %@",[data description]);
    
    NSString *urlPicture = [NSString stringWithFormat:@"%@",[profileData objectForKey:@"picture"]];
    NSURL *url = [NSURL URLWithString:urlPicture];
    NSData *dataImage = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:dataImage];
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.layer.borderWidth = 3.0f;
    self.profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileImage.clipsToBounds = YES;
    
    [self.profileImage setImage:image];
    
    profileName.text = [profileData objectForKey:@"username"];
    
    [profileXPbar setTransform:CGAffineTransformMakeScale(1.0, 7.0)];

    [profileXPbar setProgressImage:[[UIImage imageNamed:@"app_83"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 100, 0)]]; //เทา

    profileXPbar.layer.cornerRadius = 10.0f;
    profileXPbar.clipsToBounds = YES;
    
    [self getScorePointLevel];
}

-(void)viewDidAppear:(BOOL)animated
{
    score = [profile_percent integerValue]; //50;
    
    [BottomDialChart1 setupWithCount:1 TotalValue:100 LineWidth:10];
    [BottomDialChart1 setChartDataSource:self];
    [BottomDialChart1 setChartDelegate:self];
    [BottomDialChart1 reloadDialWithAnimation:NO];
}

-(void)getScorePointLevel
{
    NSString *urlString = [NSString stringWithFormat:@"service/user-level/%@",[profileData objectForKey:@"uid"]];
    NSLog(@"%@",urlString);
    
    NSDictionary *dataDict = (NSDictionary*)[RESTAPI syncGET:urlString];
    
    NSString *pointsString = [NSString stringWithFormat:@"%d แต้ม", [[dataDict objectForKey:@"current_point"]  intValue]];
    
    NSString *levelString = [NSString stringWithFormat:@"LEVEL %d", [[dataDict objectForKey:@"current_level"]  intValue]];
    
    profile_percent = [NSString stringWithFormat:@"%d", [[dataDict objectForKey:@"current_performance_percent"]  intValue]];
    
    NSString *xpString = [NSString stringWithFormat:@"%d/%d XP", [[dataDict objectForKey:@"current_xp"]  intValue], [[dataDict objectForKey:@"max_xp"] intValue]];
    
    self.xpBar = [dataDict objectForKey:@"current_xp_percent"];
    
    float xp = [[dataDict objectForKey:@"current_xp_percent"]  floatValue] / 100;
    
    profilePoint.text = pointsString;
    profileLevel.text = levelString;
    [profilePercent setTitle:profile_percent forState:UIControlStateNormal];
    profileXP.text = xpString;
    
    self.profileXPbar.progress = xp;
}

- (IBAction)editProfile:(id)sender
{
    EditProfileViewController *editProfileView = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    
    editProfileView.edUID = [NSString stringWithFormat:@"%@",[profileData objectForKey:@"uid"]];
    editProfileView.edImage = [NSString stringWithFormat:@"%@",[profileData objectForKey:@"picture"]];
    editProfileView.edName = self.profileName.text;
    editProfileView.edLevel = self.profileLevel.text;
    editProfileView.edPoint = self.profilePoint.text;
    
    [self.navigationController pushViewController:editProfileView animated:YES];
}

- (IBAction)viewPoint:(id)sender
{
    ProfilePointViewController *profilePointView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilePointViewController"];
    
    profilePointView.ppUID = [NSString stringWithFormat:@"%@",[profileData objectForKey:@"uid"]];
    profilePointView.ppImage = [NSString stringWithFormat:@"%@",[profileData objectForKey:@"picture"]];
    profilePointView.ppName = self.profileName.text;
    profilePointView.ppLevel = self.profileLevel.text;
    profilePointView.ppPoint = self.profilePoint.text;
    profilePointView.ppXPBar = self.xpBar;
    profilePointView.ppXPString = self.profileXP.text;
    
    [self.navigationController pushViewController:profilePointView animated:YES];
}

- (IBAction)viewScore:(id)sender
{
    ProfileScoreViewController *profileScoreView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileScoreViewController"];
    
    profileScoreView.psUID = [NSString stringWithFormat:@"%@",[profileData objectForKey:@"uid"]];
    profileScoreView.psImage = [NSString stringWithFormat:@"%@",[profileData objectForKey:@"picture"]];
    profileScoreView.psName = self.profileName.text;
    profileScoreView.psLevel = self.profileLevel.text;
    profileScoreView.psPoint = self.profilePoint.text;
    profileScoreView.psXPBar = self.xpBar;
    profileScoreView.psXPString = self.profileXP.text;
    
    [self.navigationController pushViewController:profileScoreView animated:YES];
}


#pragma NUDialChart Datasource
- (NSNumber*) dialChart:(NUDialChart*) dialChart valueOfCircleAtIndex:(int) _index
{
    return [NSNumber numberWithInteger:score];
}

- (UIColor* ) dialChart:(NUDialChart*) dialChart colorOfCircleAtIndex:(int) _index
{
    return [UIColor whiteColor];
}

- (NSString* ) dialChart:(NUDialChart*) dialChart textOfCircleAtIndex:(int) _index
{
    return [NSString stringWithFormat:@"nestle school"];
}

- (UIColor* ) dialChart:(NUDialChart*) dialChart textColorOfCircleAtIndex:(int) _index
{
    return [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0];
}

- (BOOL) dialChart:(NUDialChart*) dialChart defaultCircleAtIndex:(int) _index
{
    if ( _index == 1 )
        return YES;
    
    return NO;
}

- (UIColor*) centerTextColorInDialChart:(NUDialChart *)dialChart
{
    return [UIColor blackColor];
}


#pragma mark - NUDialChart Delegate
//- (void) touchNuDialChart:(NUDialChart *)chart
//{
//    [BottomDialChart1 reloadDialWithAnimation:YES];
//}

@end
