//
//  RESTAPI.h
//  Nestle School
//
//  Created by MOREMO on 9/27/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RESTAPI : NSObject

+ (RESTAPI *)sharedInstance;

@property (nonatomic,strong) NSString *token;

+(NSDictionary*)postLogin:(NSDictionary*)datauser;
+(NSDictionary*)postLoginFB:(NSDictionary*)datauser;

+(NSDictionary*)postRegis:(NSDictionary*)datauser;
+(NSDictionary*)postRegisFB:(NSDictionary*)datauser;

+(NSDictionary*)postEditPro:(NSDictionary*)datauser;

+(NSDictionary*)postAnsExe:(NSDictionary*)datauser;

+(NSDictionary*)syncPOST:(NSString*)service
              parameters:(NSDictionary*)datauser;

+(NSArray*)syncGET:(NSString*)service;


@end

//        RESTAPI *authen = [RESTAPI sharedInstance];
//        authen.token = result[@"uid"];
//        NSLog(@"Local UID : %@", authen.token);
