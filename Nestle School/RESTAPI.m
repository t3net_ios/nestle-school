//
//  RESTAPI.m
//  Nestle School
//
//  Created by MOREMO on 9/27/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "RESTAPI.h"

#define SERVICE_URL @"http://128.199.80.37/"

#define SERVICE_LOGIN @"service/login"
#define SERVICE_REGIS @"service/register"

#define LOGIN_FB @"service/login-facebook"
#define REGIS_FB @"service/register-facebook"

#define EDIT_PRO @"service/edit-profile"

#define ANS_EXE @"service/answer-quizzes"

@implementation RESTAPI


+ (RESTAPI *)sharedInstance
{
    static dispatch_once_t once;
    static RESTAPI *sharedInstance;
    
    dispatch_once(&once, ^{
        sharedInstance = [[RESTAPI alloc] init];
    });
    
    return sharedInstance;
}



/////***POST LOGIN***/////
+(NSDictionary*)postLogin:(NSDictionary*)datauser
{
    NSDictionary *headers = @{@"content-type":@"application/json"};
    
    NSDictionary *parameters = datauser;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString *urlString = [SERVICE_URL stringByAppendingString:SERVICE_LOGIN];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if (data)
    {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableLeaves
                                                                 error:&error];
        
        
        NSLog(@"LoginFBResponse %@",[result description]);
        
        [[NSUserDefaults standardUserDefaults] setObject:result forKey:@"profile"];
        
        return result;
    }
    
    return nil;
}
/////***POST LOGIN***/////



/////***POST LOGINFB***/////
+(NSDictionary*)postLoginFB:(NSDictionary*)datauser
{
    NSDictionary *headers = @{@"content-type":@"application/json"};
    
    NSDictionary *parameters = datauser;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString *urlString = [SERVICE_URL stringByAppendingString:LOGIN_FB];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if (data)
    {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableLeaves
                                                                 error:&error];
        
        
        NSLog(@"LoginFBResponse %@",[result description]);

        [[NSUserDefaults standardUserDefaults] setObject:result forKey:@"profile"];
        
        return result;
    }
    
    return nil;
}
/////***POST LOGINFB***/////



/////***POST REGISTER***/////
+(NSDictionary*)postRegis:(NSDictionary*)datauser
{
    NSDictionary *headers = @{@"content-type":@"application/json"};
    
    NSDictionary *parameters = datauser;
    
    //Eakapong
    [[NSUserDefaults standardUserDefaults] setObject:datauser forKey:@"datauser"];
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString *urlString = [SERVICE_URL stringByAppendingString:SERVICE_REGIS];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if (data)
    {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableLeaves
                                                                 error:&error];
        
        
        NSLog(@"RegisResponse %@",[result description]);
        
        return result;
    }
    
    return nil;
}
/////***POST REGISTER***/////



/////***POST REGISTERFB**/////
+(NSDictionary*)postRegisFB:(NSDictionary*)datauser
{
    NSDictionary *headers = @{@"content-type":@"application/json"};
    
    NSDictionary *parameters = datauser;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString *urlString = [SERVICE_URL stringByAppendingString:REGIS_FB];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if (data)
    {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableLeaves
                                                                 error:&error];
        
        
        NSLog(@"RegisFBResponse %@",[result description]);
        
        return result;
    }
    
    return nil;
}
/////***POST REGISTERFB**/////



/////***POST EDITPRO***/////
+(NSDictionary*)postEditPro:(NSDictionary*)datauser
{
    NSDictionary *headers = @{@"content-type":@"application/json"};
    
    NSDictionary *parameters = datauser;
    
    //Eakapong
    [[NSUserDefaults standardUserDefaults] setObject:datauser forKey:@"datauser"];
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString *urlString = [SERVICE_URL stringByAppendingString:EDIT_PRO];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if (data)
    {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableLeaves
                                                                 error:&error];
        
        
        NSLog(@"EditProResponse %@",[result description]);
        
        return result;
    }
    
    return nil;
}
/////***POST EDITPRO***/////


/////***POST ANSEXE**/////
+(NSDictionary*)postAnsExe:(NSDictionary*)datauser
{
    NSDictionary *headers = @{@"content-type":@"application/json"};
    
    NSDictionary *parameters = datauser;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString *urlString = [SERVICE_URL stringByAppendingString:ANS_EXE];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if (data)
    {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableLeaves
                                                                 error:&error];
        
        
        NSLog(@"AnsExeResponse %@",[result description]);
        
        return result;
    }
    
    return nil;
}
/////***POST ANSEXE***/////



/////***METHOD POST***/////
+(NSDictionary*)syncPOST:(NSString*)service
              parameters:(NSDictionary*)datauser
{
    NSDictionary *headers = @{@"content-type":@"application/json"};
    
    NSDictionary *parameters = datauser;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString *urlString = [SERVICE_URL stringByAppendingString:service];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if (data)
    {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableLeaves
                                                                 error:&error];
        
        
        NSLog(@"syncPOSTResponse %@",[result description]);
        
        return result;
    }
    
    return nil;

}
/////***METHOD POST***/////



/////***METHOD GET***/////
+(NSArray*)syncGET:(NSString*)service
{
    NSString *urlString = [SERVICE_URL stringByAppendingString:service];
    
    //1.Set URLRequest
    NSMutableURLRequest *request =
    [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                            cachePolicy:NSURLRequestReloadIgnoringCacheData
                        timeoutInterval:6000.0];
    
    [request setHTTPMethod:@"GET"];
    
    //2.Sent Connection
    NSError        *error = nil;
    NSURLResponse  *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if(data)
    {
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableLeaves
                                                                 error:&error];
        
        //NSLog(@"syncGETResponse %@",[result description]);
        
        return result;
    }
    
    return nil;
}
/////***METHOD GET***/////



@end
