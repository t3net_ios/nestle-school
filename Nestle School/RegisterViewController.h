//
//  RegisterViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/21/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSArray *arrayResult;
    
    NSArray *prefixArray;
    NSArray *gradeArray;
    NSArray *provinceArray;
    
    NSString *gradeInt;
}

@property (weak, nonatomic) IBOutlet UILabel *registerlbl;

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (weak, nonatomic) IBOutlet UITextField *usernameTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (weak, nonatomic) IBOutlet UITextField *confirmTxt;

@property (weak, nonatomic) IBOutlet UITextField *prefixTxt;
@property (weak, nonatomic) IBOutlet UITextField *nameTxt;
@property (weak, nonatomic) IBOutlet UITextField *bdTxt;
@property (weak, nonatomic) IBOutlet UITextField *schoolTxt;
@property (weak, nonatomic) IBOutlet UITextField *gradeTxt;
@property (weak, nonatomic) IBOutlet UITextField *provinceTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;

@property (weak, nonatomic) IBOutlet UIImageView *bgPass;
@property (weak, nonatomic) IBOutlet UIImageView *bgPassCon;

@property (nonatomic, strong) NSString *nameImage;
@property (nonatomic, strong) NSString *dataImage;
@property (nonatomic, strong) NSString *dataResponse;

@property (nonatomic, strong) NSString *nameFB;
@property (nonatomic, strong) NSString *emailFB;
@property (nonatomic, strong) NSString *urlPhotoRG;

-(IBAction)selectSubject:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *selectedSub;
@property (weak, nonatomic) IBOutlet UIButton *selectSub1;
@property (weak, nonatomic) IBOutlet UIButton *selectSub2;
@property (weak, nonatomic) IBOutlet UIButton *selectSub3;
@property (weak, nonatomic) IBOutlet UIButton *selectSub4;
@property (weak, nonatomic) IBOutlet UIButton *selectSub5;
@property (weak, nonatomic) IBOutlet UIButton *selectSub6;
@property (weak, nonatomic) IBOutlet UIButton *selectSub7;
@property (weak, nonatomic) IBOutlet UIButton *selectSub8;
@property (weak, nonatomic) IBOutlet UIButton *selectSub9;

@property (weak, nonatomic) IBOutlet UILabel *idFBlbl;
@property (nonatomic, strong) NSString *idFB;
@property (nonatomic, strong) NSString *tkFBRG;
@property (nonatomic, strong) NSString *tkFB;

@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

@property (strong, nonatomic) IBOutlet UIPickerView *dataPicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;


- (IBAction)selectProfile:(id)sender;

- (IBAction)registerComplete:(id)sender;

- (IBAction)textFieldDismiss:(id)sender;

@end



