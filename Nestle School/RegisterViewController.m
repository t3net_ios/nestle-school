//
//  RegisterViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/21/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "RegisterViewController.h"

#import "RESTAPI.h"

#import "LoginViewController.h"
#import "ClassViewController.h"
#import "Quartzcore/Quartzcore.h"

#import "SCLAlertView.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface RegisterViewController ()
@end

@implementation RegisterViewController

@synthesize registerlbl;

@synthesize usernameTxt, passwordTxt, confirmTxt;
@synthesize prefixTxt, nameTxt, bdTxt, schoolTxt, gradeTxt, provinceTxt, emailTxt;

@synthesize profileImage, nameImage, dataImage, dataResponse, urlPhotoRG;

@synthesize idFBlbl, idFB, tkFBRG, tkFB, nameFB, emailFB;
@synthesize bgPass, bgPassCon;

@synthesize selectedSub;
@synthesize selectSub1, selectSub2, selectSub3, selectSub4, selectSub5, selectSub6, selectSub7, selectSub8, selectSub9;

@synthesize dataPicker, datePicker;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    registerlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    registerlbl.layer.shadowOffset = CGSizeMake(0,1);
    registerlbl.layer.shadowOpacity = 1.0;
    registerlbl.layer.shadowRadius = 1.0;

    
    //////////////////////////////////////////////////
    prefixArray = @[@"กรุณาเลือก", @"เด็กหญิง", @"เด็กชาย", @"นางสาว", @"นาย"];
    gradeArray = @[@"กรุณาเลือก", @"มัธยมศึกษาปีที่ 1", @"มัธยมศึกษาปีที่ 2", @"มัธยมศึกษาปีที่ 3"];
    provinceArray = @[@"กรุณาเลือก", @"กรุงเทพมหานคร", @"กระบี่", @"กาญจนบุรี", @"กาฬสินธุ์", @"กำแพงเพชร", @"ขอนแก่น", @"จันทบุรี", @"ฉะเชิงเทรา", @"ชลบุรี", @"ชัยนาท", @"ชัยภูมิ", @"ชุมพร", @"เชียงราย" ,@"เชียงใหม่", @"ตรัง", @"ตราด", @"ตาก", @"นครนายก", @"นครปฐม", @"นครพนม", @"นครราชสีมา", @"นครศรีธรรมราช", @"นครสวรรค์", @"นนทบุรี", @"นราธิวาส", @"น่าน", @"บึงกาฬ", @"บุรีรัมย์", @"ปทุมธานี", @"ประจวบคีรีขันธ์", @"ปราจีนบุรี", @"ปัตตานี", @"พระนครศรีอยุธยา", @"พังงา", @"พัทลุง", @"พิจิตร", @"พิษณุโลก", @"เพชรบุรี", @"เพชรบูรณ์", @"แพร่", @"พะเยา", @"ภูเก็ต", @"มหาสารคาม", @"มุกดาหาร", @"แม่ฮ่องสอน", @"ยะลา", @"ยโสธร", @"ร้อยเอ็ด", @"ระนอง", @"ระยอง", @"ราชบุรี", @"ลพบุรี", @"ลำปาง", @"ลำพูน", @"เลย", @"ศรีสะเกษ", @"สกลนคร", @"สงขลา", @"สตูล", @"สมุทรปราการ", @"สมุทรสงคราม", @"สมุทรสาคร", @"สระแก้ว", @"สระบุรี", @"สิงห์บุรี", @"สุโขทัย", @"สุพรรณบุรี", @"สุราษฎร์ธานี", @"สุรินทร์", @"หนองคาย", @"หนองบัวลำภู", @"อ่างทอง", @"อุดรธานี", @"อุทัยธานี", @"อุตรดิตถ์", @"อุบลราชธานี", @"อำนาจเจริญ" ];
    

    datePicker.datePickerMode = UIDatePickerModeDate;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    [comps setYear:-13];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [comps setYear:-33];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:maxDate];
    [datePicker setMinimumDate:minDate];
    
    [self.bdTxt setInputView:datePicker];
    
    [self.prefixTxt setInputView:dataPicker];
    [self.gradeTxt setInputView:dataPicker];
    [self.provinceTxt setInputView:dataPicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor blackColor]];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                style:UIBarButtonItemStyleDone
                                                               target:self
                                                               action:@selector(selectedDate)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:nil
                                                                           action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn, nil]];
    
    [self.prefixTxt setInputAccessoryView:toolBar];
    [self.gradeTxt setInputAccessoryView:toolBar];
    [self.provinceTxt setInputAccessoryView:toolBar];
    
    [self.bdTxt setInputAccessoryView:toolBar];
    
    idFBlbl.text = idFB;
    
    if (self.idFBlbl.text.length != 0)
    {
        emailTxt.text = emailFB;
        usernameTxt.text = nameFB;
        usernameTxt.enabled = NO;
        
        passwordTxt.hidden = YES;
        bgPass.hidden = YES;
        confirmTxt.hidden = YES;
        bgPassCon.hidden = YES;
        
        NSString *urlPhotoFB = urlPhotoRG;
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlPhotoFB]];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
        self.profileImage.layer.borderWidth = 3.0f;
        self.profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
        self.profileImage.clipsToBounds = YES;
        
        self.profileImage.image = image;
    }
}

- (void)selectedDate
{
    if([bdTxt isFirstResponder])
    {
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"MM/dd/yyyy"];
        self.bdTxt.text = [NSString stringWithFormat:@"%@", [dateformatter stringFromDate:datePicker.date]];
        [self.bdTxt resignFirstResponder];
    }
    
    [self.prefixTxt resignFirstResponder];
    [self.gradeTxt resignFirstResponder];
    [self.provinceTxt resignFirstResponder];
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)picker
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if([prefixTxt isFirstResponder])
        return prefixArray.count;
    
    if([gradeTxt isFirstResponder])
        return gradeArray.count;
    
    else
        return provinceArray.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([prefixTxt isFirstResponder])
        return [prefixArray objectAtIndex:row];
    
    if([gradeTxt isFirstResponder])
        return [gradeArray objectAtIndex:row];
    
    else
        return [provinceArray objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([prefixTxt isFirstResponder])
    {
        prefixTxt.text = prefixArray[row];
        
    } else if([gradeTxt isFirstResponder]) {
        
        gradeTxt.text = gradeArray[row];
        
    } else {
        
        provinceTxt.text = provinceArray[row];
        
    }
 }

/////***CHOOSEPHOTO***/////
- (IBAction)selectProfile:(id)sender
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:@""
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction *takePhoto = [UIAlertAction
                         actionWithTitle:@"Take Photo"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                             picker.delegate = self;
                             picker.allowsEditing = YES;
                             picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                             
                             [self presentViewController:picker animated:YES completion:NULL];
                             
                         }];
    
    UIAlertAction *selectPhoto = [UIAlertAction
                             actionWithTitle:@"Select Photo"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                 picker.delegate = self;
                                 picker.allowsEditing = YES;
                                 picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                 
                                 [self presentViewController:picker animated:YES completion:NULL];

                             }];
    
    UIAlertAction *Cancel = [UIAlertAction
                                  actionWithTitle:@"Cancel"
                                  style:UIAlertActionStyleCancel
                                  handler:^(UIAlertAction * action)
                                  {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                  }];

    
    [alert addAction:takePhoto];
    [alert addAction:selectPhoto];
    [alert addAction:Cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];

    UIImage *resizedImage = [self imageWithImage:image scaledToSize:CGSizeMake(300, 300)];
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.layer.borderWidth = 3.0f;
    self.profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileImage.clipsToBounds = YES;
    
    self.profileImage.image = resizedImage;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
/////***CHOOSEPHOTO***/////



/////***POST BY USER***/////
- (void)registerbyUser
{
    UIImage *image = [UIImage imageWithData:UIImagePNGRepresentation(profileImage.image)];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    dataImage = [@"data:image/jpeg;base64," stringByAppendingString:[imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]];
    
    nameImage = [NSString stringWithFormat:@"%@.jpg",self.usernameTxt.text];
    
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 1"])
    {
        gradeInt = @"1";
    }
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 2"])
    {
        gradeInt = @"2";
    }
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 3"])
    {
        gradeInt = @"3";
    }
    
    NSDictionary *datauser = @{ @"username": usernameTxt.text,
                                @"password": passwordTxt.text,
                                @"mail": emailTxt.text,
                                @"fullname": nameTxt.text,
                                @"class": gradeInt,
                                @"interest": selectedSub.text,
                                @"prefix": prefixTxt.text,
                                @"province": provinceTxt.text,
                                @"birthday": bdTxt.text,
                                @"school": schoolTxt.text,
                                @"nameImage": nameImage,
                                @"image": dataImage };
    
    NSDictionary *result = [RESTAPI postRegis:datauser];

    if([result[@"status"] isEqualToString:@"success"])
    {
        NSDictionary *data = @{ @"user": usernameTxt.text,
                                @"pass": passwordTxt.text };
        
        NSDictionary *re_result = [RESTAPI postLogin:data];
        
        arrayResult = re_result[@"uid"];

        if (arrayResult != nil)
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:@"registered"];
            [defaults synchronize];

            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"registered"])
            {
                SCLAlertView *alert = [[SCLAlertView alloc] init];
                
                [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
                [alert setBodyTextFontFamily:@"DBSaiKrokX" withSize:23.0f];
                [alert setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
                
                [alert addButton:@"เข้าเรียน"
                          target:self
                        selector:@selector(alertComplete)];
                
                [alert showCustom:self
                            image:[UIImage imageNamed:@"app_01"]
                            color:Color
                            title:@"สวัสดีนักเรียน"
                         subTitle:@"ยินดีต้อนรับสู่ Nestle' School โรงเรียนนี้จะช่วยละลายความยากให้ทุกเรียน"
                 closeButtonTitle:nil
                         duration:0.0f];
            }
        }
        
    } else {

        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
        [alert setBodyTextFontFamily:@"DBSaiKrokX" withSize:25.0f];
        [alert setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
        
        [alert showCustom:self
                    image:[UIImage imageNamed:@"app_01"]
                    color:Color
                    title:@"ลงทะเบียน ไม่สำเร็จ"
                 subTitle:@"กรุณากรอกข้อมูลส่วนตัวใหม่อีกครั้ง"
         closeButtonTitle:@"OK"
                 duration:0.0f];
    }
}
/////***POST BY USER***/////



/////***POST BY FB***/////
- (void)registerbyFB
{
    UIImage *image = [UIImage imageWithData:UIImagePNGRepresentation(profileImage.image)];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    dataImage = [@"data:image/jpeg;base64," stringByAppendingString:[imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]];
    
    nameImage = [NSString stringWithFormat:@"%@.jpg",self.usernameTxt.text];
    tkFBRG = tkFB;
    
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 1"])
    {
        gradeInt = @"1";
    }
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 2"])
    {
        gradeInt = @"2";
    }
    if ([gradeTxt.text isEqualToString:@"มัธยมศึกษาปีที่ 3"])
    {
        gradeInt = @"3";
    }
    
    NSDictionary *datauser = @{ @"nestleRF": @"registerFacebook",
                                @"username": idFBlbl.text,
                                @"mail": emailTxt.text,
                                @"fullname": nameTxt.text,
                                @"class": gradeInt,
                                @"interest": selectedSub.text,
                                @"prefix": prefixTxt.text,
                                @"province": provinceTxt.text,
                                @"birthday": bdTxt.text,
                                @"school": schoolTxt.text,
                                @"nameImage": nameImage,
                                @"facebook_token": tkFBRG,
                                @"facebook_id": idFBlbl.text,
                                @"image": dataImage };
    
    NSDictionary *result = [RESTAPI postRegisFB:datauser];
    
    if([result[@"status"] isEqualToString:@"success"])
    {
        arrayResult = result[@"uid"];
        
        if (arrayResult != nil)
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:@"registered"];
            [defaults synchronize];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"registered"])
            {
                NSDictionary *datauser = @{ @"nestleLF": @"loginFacebook",
                                            @"facebook_id": idFBlbl.text };
                
                NSDictionary *resultFB = [RESTAPI postLoginFB:datauser];
                
                NSLog(@"%@", resultFB);
                
                [self alertComplete];
            }
        }
        
    } else {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
        [alert setBodyTextFontFamily:@"DBSaiKrokX" withSize:25.0f];
        [alert setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
        
        [alert showCustom:self
                    image:[UIImage imageNamed:@"app_01"]
                    color:Color
                    title:@"ลงทะเบียน ไม่สำเร็จ"
                 subTitle:@"กรุณากรอกข้อมูลส่วนตัวใหม่อีกครั้ง"
         closeButtonTitle:@"OK"
                 duration:0.0f];
    }
}
/////***POST BY FB***/////



/////***CHECKBLANK***/////
- (void)checkBlankByUser
{
    if ([self.usernameTxt.text isEqualToString:@""] ||
        [self.emailTxt.text isEqualToString:@""] ||
        [self.nameTxt.text isEqualToString:@""] ||
        [self.gradeTxt.text isEqualToString:@""] ||
        [self.prefixTxt.text isEqualToString:@""] ||
        [self.provinceTxt.text isEqualToString:@""] ||
        [self.bdTxt.text isEqualToString:@""] ||
        [self.schoolTxt.text isEqualToString:@""] ||
        [self.passwordTxt.text isEqualToString:@""] ||
        [self.confirmTxt.text isEqualToString:@""] ||
        [self.selectedSub.text isEqualToString:@""])
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
        [alert setBodyTextFontFamily:@"DBSaiKrokX" withSize:25.0f];
        [alert setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
        
        [alert showCustom:self
                    image:[UIImage imageNamed:@"app_01"]
                    color:Color
                    title:@"ลงทะเบียน ไม่สำเร็จ"
                 subTitle:@"กรุณากรอกข้อมูลส่วนตัวให้ครบถ้วน"
         closeButtonTitle:@"OK"
                 duration:0.0f];

    } else {
        
        [self registerbyUser];

    }
}

- (void)checkBlankByFB
{
    if ([self.usernameTxt.text isEqualToString:@""] ||
        [self.emailTxt.text isEqualToString:@""] ||
        [self.nameTxt.text isEqualToString:@""] ||
        [self.gradeTxt.text isEqualToString:@""] ||
        [self.prefixTxt.text isEqualToString:@""] ||
        [self.provinceTxt.text isEqualToString:@""] ||
        [self.bdTxt.text isEqualToString:@""] ||
        [self.schoolTxt.text isEqualToString:@""] ||
        [self.selectedSub.text isEqualToString:@""])
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
        [alert setBodyTextFontFamily:@"DBSaiKrokX" withSize:25.0f];
        [alert setButtonsTextFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
        
        [alert showCustom:self
                    image:[UIImage imageNamed:@"app_01"]
                    color:Color
                    title:@"ลงทะเบียน ไม่สำเร็จ"
                 subTitle:@"กรุณากรอกข้อมูลส่วนตัวให้ครบถ้วน"
         closeButtonTitle:@"OK"
                 duration:0.0f];
        
    } else {
    
        [self registerbyFB];
        
    }
}
/////***CHECKBLANK***/////



-(void)alertComplete
{
    UIViewController *storyboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                    instantiateViewControllerWithIdentifier:@"TutorialViewController"];
    
    [self presentViewController:storyboard animated:YES completion:nil];
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UITabBarController *tabcontroller = [mainStoryboard instantiateViewControllerWithIdentifier:@"TabBarController"];
//    
//    tabcontroller.selectedIndex = 2;
//    
//    UITabBar *tabBar = tabcontroller.tabBar;
//    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
//    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
//    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
//    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
//    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
//    
//    tabBarItem1.selectedImage = [[UIImage imageNamed:@"ico_1_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    tabBarItem1.image = [[UIImage imageNamed:@"ico_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    tabBarItem2.selectedImage = [[UIImage imageNamed:@"ico_2_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    tabBarItem2.image = [[UIImage imageNamed:@"ico_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    tabBarItem3.selectedImage = [[UIImage imageNamed:@"BUTTON3-2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    tabBarItem3.image = [[UIImage imageNamed:@"BUTTON3.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    tabBarItem4.selectedImage = [[UIImage imageNamed:@"ico_3_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    tabBarItem4.image = [[UIImage imageNamed:@"ico_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    tabBarItem5.selectedImage = [[UIImage imageNamed:@"ico_4_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    tabBarItem5.image = [[UIImage imageNamed:@"ico_4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    //[self.navigationController pushViewController:tabcontroller animated:YES];
//    [self presentViewController:tabcontroller animated:YES completion:nil];
}



/////***REGISTER***////
- (IBAction)registerComplete:(id)sender
{
    if (self.idFBlbl.text.length == 0)
    {
        [self checkBlankByUser];
        
    } else {
        
        [self checkBlankByFB];
    }
}
/////***REGISTER***/////



/////***SELECTEDSUB***/////
- (IBAction)selectSubject:(id)sender
{
    switch ([sender tag])
    {
        case 1:
            if ([selectSub1 isSelected]==YES)
            {
                [selectSub1 setSelected:NO];
            
            } else {
                
                [selectSub1 setSelected:YES];
                //[selectSub2 setSelected:NO];
                
                [selectSub1 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub1 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub1 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 2:
            if ([selectSub2 isSelected]==YES)
            {
                [selectSub2 setSelected:NO];
                
            } else {
                
                [selectSub2 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub2 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub2 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub2 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 3:
            if ([selectSub3 isSelected]==YES)
            {
                [selectSub3 setSelected:NO];
                
            } else {
                
                [selectSub3 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub3 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub3 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub3 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;

        case 4:
            if ([selectSub4 isSelected]==YES)
            {
                [selectSub4 setSelected:NO];
                
            } else {
                
                [selectSub4 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub4 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub4 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub4 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 5:
            if ([selectSub5 isSelected]==YES)
            {
                [selectSub5 setSelected:NO];
                
            } else {
                
                [selectSub5 setSelected:YES];
                //[selectSub1 setSelected:NO];

                [selectSub5 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub5 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub5 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 6:
            if ([selectSub6 isSelected]==YES)
            {
                [selectSub6 setSelected:NO];
                
            } else {
                
                [selectSub6 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub6 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub6 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub6 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 7:
            if ([selectSub7 isSelected]==YES)
            {
                [selectSub7 setSelected:NO];
                
            } else {
                
                [selectSub7 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub7 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub7 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub7 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;
            
        case 8:
            if ([selectSub8 isSelected]==YES)
            {
                [selectSub8 setSelected:NO];
                
            } else {
                
                [selectSub8 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub8 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub8 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub8 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;

        case 9:
            if ([selectSub9 isSelected]==YES)
            {
                [selectSub9 setSelected:NO];
                
            } else {
                
                [selectSub9 setSelected:YES];
                //[selectSub1 setSelected:NO];
                
                [selectSub9 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                [selectSub9 setTitleColor:Color
                                 forState:UIControlStateSelected];
                selectedSub.text = [selectSub9 titleForState:UIControlStateNormal];
                //NSLog(@"SelectedSub : %@",selectedSub);
            }
            break;

        default:
            break;
    }
}
/////***SELECTEDSUB***/////



- (IBAction)textFieldDismiss:(id)sender
{
    [usernameTxt resignFirstResponder];
    [passwordTxt resignFirstResponder];
    [confirmTxt resignFirstResponder];
    
    [prefixTxt resignFirstResponder];
    [nameTxt resignFirstResponder];
    [bdTxt resignFirstResponder];
    [schoolTxt resignFirstResponder];
    [gradeTxt resignFirstResponder];
    [provinceTxt resignFirstResponder];
    [emailTxt resignFirstResponder];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}



- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:^{
        //
    }];
    
    [self.navigationController popViewControllerAnimated:NO];
}

@end




