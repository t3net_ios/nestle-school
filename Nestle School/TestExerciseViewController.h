//
//  TestExerciseViewController.h
//  Nestle School
//
//  Created by MOREMO on 10/19/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestExerciseViewController : UIViewController
{    
    NSArray *questionArray;
    NSMutableArray *answerArray;
    
    int currentQuestionIndex;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *answerView;

@property (weak, nonatomic) IBOutlet UILabel *testlbl;

@property (weak, nonatomic) IBOutlet UILabel *testTitle;
@property (weak, nonatomic) IBOutlet UILabel *testNumber;
@property (weak, nonatomic) IBOutlet UILabel *testTime;

@property (weak, nonatomic) IBOutlet UILabel *testNumberS;
@property (weak, nonatomic) IBOutlet UILabel *testNumberAll;

@property (weak, nonatomic) IBOutlet UITextView *question;

@property (weak, nonatomic) IBOutlet UIButton *choice1;
@property (weak, nonatomic) IBOutlet UIButton *choice2;
@property (weak, nonatomic) IBOutlet UIButton *choice3;
@property (weak, nonatomic) IBOutlet UIButton *choice4;

@property (nonatomic, strong) NSString *nidTestExe;
@property (nonatomic, strong) NSString *titleTestExe;
@property (nonatomic, strong) NSString *numberTestExe;
@property (nonatomic, strong) NSString *timeTestExe;

@property (nonatomic, strong) NSString *dataResponse;

- (IBAction)nextQuestion:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextQuiz;

- (IBAction)previousQuestion:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *previousQuiz;

- (IBAction)choicepressed:(id)sender;
- (IBAction)back:(id)sender;

@property (nonatomic, strong) NSString *test;

@property (weak, nonatomic) IBOutlet UITextView *ans1;
@property (weak, nonatomic) IBOutlet UITextView *ans2;
@property (weak, nonatomic) IBOutlet UITextView *ans3;
@property (weak, nonatomic) IBOutlet UITextView *ans4;


@end


