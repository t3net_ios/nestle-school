//
//  TestExerciseViewController.m
//  Nestle School
//
//  Created by MOREMO on 10/19/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "TestExerciseViewController.h"
#import "RESTAPI.h"

#import "SCLAlertView.h"
#import "ProfileViewController.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]


@interface TestExerciseViewController ()
@end

@implementation TestExerciseViewController

@synthesize testlbl;

@synthesize question;
@synthesize choice1, choice2, choice3, choice4;

@synthesize testTitle, testNumber, testTime, testNumberS, testNumberAll;
@synthesize nidTestExe, titleTestExe, timeTestExe;


@synthesize scrollView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    testlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    testlbl.layer.shadowOffset = CGSizeMake(0, 1);
    testlbl.layer.shadowOpacity = 1.0;
    testlbl.layer.shadowRadius = 1.0;
    

    testTitle.numberOfLines = 0;
    testTitle.text = titleTestExe;
    [testTitle sizeToFit];
    
    testTime.text = [NSString stringWithFormat:@"00:05:00"]; //,timeTestExe];
    
    [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(tickTimer:)
                                   userInfo:nil
                                    repeats:YES];
    
    
    NSString *urlString = [NSString stringWithFormat:@"service/quizzes-detail/%@",nidTestExe];
    questionArray = [RESTAPI syncGET:urlString];
    NSLog(@"%@",urlString);
    
    answerArray = [NSMutableArray new];
    for(NSDictionary *ind in questionArray){
        [answerArray addObject:@{@"value":@"0"}] ;
    }
    
    testNumberAll.text = [NSString stringWithFormat:@"/ %lu", (unsigned long)questionArray.count];
    
    currentQuestionIndex = -1;
    [self doNextQuestion];
}

- (void)tickTimer:(NSTimer *)timer
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"HH:mm:ss";
    
    NSDate *startTime = [df dateFromString:self.testTime.text];
    
    NSDate *endTime = [NSDate dateWithTimeInterval:-1
                                         sinceDate:startTime];
    
    self.testTime.text = [df stringFromDate:endTime];
    //NSLog(@"tickTimer %@", self.testTime.text);
    
    if ([self.testTime.text isEqualToString:@"00:00:00"] == YES)
    {
        
        self.testTime.textColor = [UIColor redColor];
        
        [timer invalidate];
        
        [self answerQuizzes];
    }
}

- (void)doPreviousQuestion{
    currentQuestionIndex--;
    [self doQuestion];
}

- (void)doNextQuestion{
    currentQuestionIndex++;
    [self doQuestion];
}

- (void)doQuestion
{
    testNumber.text = [NSString stringWithFormat:@"%d", currentQuestionIndex+1];
    testNumberS.text = [NSString stringWithFormat:@"%d", currentQuestionIndex+1];
    
    NSString *field_question  = [NSString stringWithFormat:@"%@", [questionArray[currentQuestionIndex] valueForKey:@"field_question"]];
    NSString *field_choice1 = [NSString stringWithFormat:@"%@", [questionArray[currentQuestionIndex] valueForKey:@"field_choice1"]];
    NSString *field_choice2 = [NSString stringWithFormat:@"%@", [questionArray[currentQuestionIndex] valueForKey:@"field_choice2"]];
    NSString *field_choice3 = [NSString stringWithFormat:@"%@", [questionArray[currentQuestionIndex] valueForKey:@"field_choice3"]];
    NSString *field_choice4 = [NSString stringWithFormat:@"%@", [questionArray[currentQuestionIndex] valueForKey:@"field_choice4"]];

    
    //Eakapong
    field_question = [field_question stringByReplacingOccurrencesOfString:@"/sites/default/file" withString:@"http://128.199.80.37/sites/default/file"];
    
    field_question = [field_question stringByReplacingOccurrencesOfString:@"<p>" withString:@" "];
    field_question = [field_question stringByReplacingOccurrencesOfString:@"</p>" withString:@" "];

    
    NSAttributedString *field_questionString = [[NSAttributedString alloc]
                                                initWithData:[field_question dataUsingEncoding:NSUnicodeStringEncoding]
                                                options:@{ NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType }
                                                documentAttributes:nil
                                                error:nil];
    

    question.attributedText = field_questionString;
    question.font = [UIFont fontWithName:@"DBSaiKrokX-Bold" size:30.0f];
    //[question sizeToFit];
    
    CGFloat fixedWidth = question.frame.size.width;
    CGSize newSize = [question sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = question.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    question.frame = newFrame;
    
    
    
    //CHOICE1
    field_choice1 = [field_choice1 stringByReplacingOccurrencesOfString:@"/sites/default/file" withString:@"http://128.199.80.37/sites/default/file"];
    
    field_choice1 = [field_choice1 stringByReplacingOccurrencesOfString:@"<p>" withString:@" "];
    field_choice1 = [field_choice1 stringByReplacingOccurrencesOfString:@"</p>" withString:@" "];
    
    NSAttributedString *field_choice1String = [[NSAttributedString alloc]
                                                initWithData:[field_choice1 dataUsingEncoding:NSUnicodeStringEncoding]
                                                options:@{ NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType }
                                                documentAttributes:nil
                                                error:nil];
    
    //NSString *strchoice1 = [field_choice1String string];
    _ans1.attributedText = field_choice1String;
    _ans1.font = [UIFont fontWithName:@"DBSaiKrokX" size:30.0f];
    
    CGFloat fixedWidth1 = _ans1.frame.size.width;
    CGSize newSize1 = [_ans1 sizeThatFits:CGSizeMake(fixedWidth1, MAXFLOAT)];
    CGRect newFrame1 = _ans1.frame;
    newFrame1.size = CGSizeMake(fmaxf(newSize1.width, fixedWidth1), newSize1.height);
    _ans1.frame = newFrame1;
    
//    UITapGestureRecognizer *tap1  = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                            action:@selector(ansTapped:)];
//    [tap1 setNumberOfTapsRequired:1];
//    [tap1 setNumberOfTouchesRequired:1];
//    self.ans1.userInteractionEnabled = YES;
//    [self.ans1 addGestureRecognizer:tap1];
    [self.choice1 addSubview:_ans1];
    
    
    
    //CHOICE2
    field_choice2 = [field_choice2 stringByReplacingOccurrencesOfString:@"/sites/default/file" withString:@"http://128.199.80.37/sites/default/file"];
    
    field_choice2 = [field_choice2 stringByReplacingOccurrencesOfString:@"<p>" withString:@" "];
    field_choice2 = [field_choice2 stringByReplacingOccurrencesOfString:@"</p>" withString:@" "];
    
    NSAttributedString *field_choice2String = [[NSAttributedString alloc]
                                               initWithData:[field_choice2 dataUsingEncoding:NSUnicodeStringEncoding]
                                               options:@{ NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType }
                                               documentAttributes:nil
                                               error:nil];
    
    //NSString *strchoice2 = [field_choice2String string];
    _ans2.attributedText = field_choice2String;
    _ans2.font = [UIFont fontWithName:@"DBSaiKrokX" size:30.0f];
    
    CGFloat fixedWidth2 = _ans2.frame.size.width;
    CGSize newSize2 = [_ans2 sizeThatFits:CGSizeMake(fixedWidth2, MAXFLOAT)];
    CGRect newFrame2 = _ans2.frame;
    newFrame2.size = CGSizeMake(fmaxf(newSize2.width, fixedWidth2), newSize2.height);
    _ans2.frame = newFrame2;
    
//    UITapGestureRecognizer *tap2  = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                            action:@selector(ansTapped:)];
//    [tap2 setNumberOfTapsRequired:1];
//    [tap2 setNumberOfTouchesRequired:1];
//    self.ans2.userInteractionEnabled = YES;
//    [self.ans2 addGestureRecognizer:tap2];
    [self.choice2 addSubview:_ans2];
    
    
    //CHOICE3
    field_choice3 = [field_choice3 stringByReplacingOccurrencesOfString:@"/sites/default/file" withString:@"http://128.199.80.37/sites/default/file"];
    
    field_choice3 = [field_choice3 stringByReplacingOccurrencesOfString:@"<p>" withString:@" "];
    field_choice3 = [field_choice3 stringByReplacingOccurrencesOfString:@"</p>" withString:@" "];
    
    NSAttributedString *field_choice3String = [[NSAttributedString alloc]
                                               initWithData:[field_choice3 dataUsingEncoding:NSUnicodeStringEncoding]
                                               options:@{ NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType }
                                               documentAttributes:nil
                                               error:nil];
    
    //NSString *strchoice3 = [field_choice3String string];
    _ans3.attributedText = field_choice3String;
    _ans3.font = [UIFont fontWithName:@"DBSaiKrokX" size:30.0f];
    
    CGFloat fixedWidth3 = _ans3.frame.size.width;
    CGSize newSize3 = [_ans3 sizeThatFits:CGSizeMake(fixedWidth3, MAXFLOAT)];
    CGRect newFrame3 = _ans3.frame;
    newFrame3.size = CGSizeMake(fmaxf(newSize3.width, fixedWidth3), newSize3.height);
    _ans3.frame = newFrame3;
    
//    UITapGestureRecognizer *tap3  = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                            action:@selector(ansTapped:)];
//    [tap3 setNumberOfTapsRequired:1];
//    [tap3 setNumberOfTouchesRequired:1];
//    self.ans3.userInteractionEnabled = YES;
//    [self.ans3 addGestureRecognizer:tap3];
    [self.choice3 addSubview:_ans3];
    
    
    //CHOICE4
    field_choice4 = [field_choice4 stringByReplacingOccurrencesOfString:@"/sites/default/file" withString:@"http://128.199.80.37/sites/default/file"];
    
    field_choice4 = [field_choice4 stringByReplacingOccurrencesOfString:@"<p>" withString:@" "];
    field_choice4 = [field_choice4 stringByReplacingOccurrencesOfString:@"</p>" withString:@" "];

    NSAttributedString *field_choice4String = [[NSAttributedString alloc]
                                               initWithData:[field_choice4 dataUsingEncoding:NSUnicodeStringEncoding]
                                               options:@{ NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType }
                                               documentAttributes:nil
                                               error:nil];
    
    //NSString *strchoice4 = [field_choice4String string];
    _ans4.attributedText = field_choice4String;
    _ans4.font = [UIFont fontWithName:@"DBSaiKrokX" size:30.0f];
    
    CGFloat fixedWidth4 = _ans4.frame.size.width;
    CGSize newSize4 = [_ans4 sizeThatFits:CGSizeMake(fixedWidth4, MAXFLOAT)];
    CGRect newFrame4 = _ans4.frame;
    newFrame4.size = CGSizeMake(fmaxf(newSize4.width, fixedWidth4), newSize4.height);
    _ans4.frame = newFrame4;
    
//    UITapGestureRecognizer *tap4  = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                            action:@selector(ansTapped:)];
//    [tap4 setNumberOfTapsRequired:1];
//    [tap4 setNumberOfTouchesRequired:1];
//    self.ans4.userInteractionEnabled = YES;
//    [self.ans4 addGestureRecognizer:tap4];
    [self.choice4 addSubview:_ans4];

    
    
    //////////
    float questionHeight = question.frame.size.height;
    
    float ans1Height = _ans1.frame.size.height;
    float ans2Height = _ans2.frame.size.height;
    float ans3Height = _ans3.frame.size.height;
    float ans4Height = _ans4.frame.size.height;
    
    float answerHeight = self.answerView.frame.size.height ;
    
    float footerOffset = 20;
    //NSLog(@"question height %f",questionHeight);

    [self.ans1 setCenter:CGPointMake(self.view.frame.size.width/2, (ans1Height/10))];
    [self.ans2 setCenter:CGPointMake(self.view.frame.size.width/2, (ans1Height/10) + (ans2Height/10))];
    [self.ans3 setCenter:CGPointMake(self.view.frame.size.width/2, (ans1Height/10) + (ans2Height/10) + (ans3Height/10))];
    [self.ans4 setCenter:CGPointMake(self.view.frame.size.width/2, (ans1Height/10) + (ans2Height/10) + (ans3Height/10) + (ans4Height/10))];
    
    [self.answerView setCenter:CGPointMake(self.view.frame.size.width/2, questionHeight + (answerHeight/2))];
    // + ans1Height + ans2Height + ans3Height + ans4Height +
    
    //Eakapong
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,
                                          questionHeight +
                                          ans1Height +
                                          ans2Height +
                                          ans3Height +
                                          ans4Height +
                                          answerHeight +
                                          footerOffset)];
    
    scrollView.layer.borderColor = [Color CGColor];
    scrollView.layer.cornerRadius = 15.0;
    scrollView.layer.borderWidth = 3.0;
    
    
    NSDictionary *currentAnswer = [answerArray objectAtIndex:currentQuestionIndex];
    
    [choice1 setSelected:NO];
    [choice2 setSelected:NO];
    [choice3 setSelected:NO];
    [choice4 setSelected:NO];
    
    if([[currentAnswer valueForKey:@"value"] isEqualToString:@"1"]){
        [choice1 setSelected:YES];
    }
    if([[currentAnswer valueForKey:@"value"] isEqualToString:@"2"]){
        [choice2 setSelected:YES];
    }
    if([[currentAnswer valueForKey:@"value"] isEqualToString:@"3"]){
        [choice3 setSelected:YES];
    }
    if([[currentAnswer valueForKey:@"value"] isEqualToString:@"4"]){
        [choice4 setSelected:YES];
    }
}

- (IBAction)previousQuestion:(id)sender
{
    if(currentQuestionIndex == 1){
        _previousQuiz.hidden = YES;
    }
    [self doPreviousQuestion];
}

- (IBAction)nextQuestion:(id)sender
{
    if(currentQuestionIndex == 0){
    _previousQuiz.hidden = NO;
    }
    
    if (currentQuestionIndex == (questionArray.count - 1))
    {
        NSInteger total = questionArray.count;
        NSString *title = [NSString stringWithFormat:@"ทำครบ %ld ข้อสอบ", (long)total];
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        alert.backgroundViewColor = [UIColor whiteColor];
        
        [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
        [alert setBodyTextFontFamily:@"DBSaiKrokX-Bold" withSize:40.0f];
        [alert setButtonsTextFontFamily:@"DBSaiKrokX" withSize:20.0f];
        
        [alert addButton:@"ส่งคุณครู"
                  target:self
                selector:@selector(answerQuizzes)];
        
        [alert showCustom:self
                    image:[UIImage imageNamed:@"app_01"]
                    color:Color
                    title:titleTestExe
                 subTitle:title
         closeButtonTitle:@"ตรวจทานอีกรอบ"
                 duration:0.0f];
        
    } else {
        [self doNextQuestion];
    }
}

- (IBAction)choicepressed:(id)sender
{
    NSString *choiceSelect = @"0";
    switch ([sender tag])
    {
        case 1:
            if ([choice1 isSelected]==YES)
            {
                [choice1 setSelected:NO];
                
            } else {
                
                [choice1 setSelected:YES];
                [choice2 setSelected:NO];
                [choice3 setSelected:NO];
                [choice4 setSelected:NO];
                
                [choice1 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                         forState:UIControlStateSelected];
                
                [choice1 setTitleColor:Color
                              forState:UIControlStateSelected];
                
                choiceSelect = @"1";
            }
            break;
            
        case 2:
            if ([choice2 isSelected]==YES)
            {
                [choice2 setSelected:NO];
                
            } else {
                
                [choice2 setSelected:YES];
                [choice1 setSelected:NO];
                [choice3 setSelected:NO];
                [choice4 setSelected:NO];
                
                [choice2 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                         forState:UIControlStateSelected];
                
                [choice2 setTitleColor:Color
                              forState:UIControlStateSelected];
                
                choiceSelect = @"2";
            }
            break;
            
        case 3:
            if ([choice3 isSelected]==YES)
            {
                [choice3 setSelected:NO];
                
            } else {
                
                [choice3 setSelected:YES];
                [choice1 setSelected:NO];
                [choice2 setSelected:NO];
                [choice4 setSelected:NO];
                
                [choice3 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                         forState:UIControlStateSelected];
                
                [choice3 setTitleColor:Color
                              forState:UIControlStateSelected];
                
                choiceSelect = @"3";
            }
            break;

        case 4:
            if ([choice4 isSelected]==YES)
            {
                [choice4 setSelected:NO];
                
            } else {
                
                [choice4 setSelected:YES];
                [choice1 setSelected:NO];
                [choice2 setSelected:NO];
                [choice3 setSelected:NO];
                
                [choice4 setImage:[UIImage imageNamed:@"TIKBOX-V2-2.png"]
                            forState:UIControlStateSelected];
                
                [choice4 setTitleColor:Color
                              forState:UIControlStateSelected];
                
                choiceSelect = @"4";
            }
            break;
            
        default:
            break;
    }

    [answerArray replaceObjectAtIndex:currentQuestionIndex
                           withObject:@{@"value":choiceSelect}] ;
}

- (void)answerQuizzes
{
    NSDictionary *profileData = [[NSUserDefaults standardUserDefaults] valueForKey:@"profile"];
    NSString *uid = [profileData objectForKey:@"uid"];
    
    NSDictionary *datauser = @{ @"uid": uid,
                                @"field_answer" :answerArray,
                                @"field_quizzes_answer" : nidTestExe };
    
    NSLog(@"answerQuizzes %@",[datauser description]);
    
    NSDictionary *result = [RESTAPI postAnsExe:datauser];
    
    if([result[@"status"] isEqualToString:@"success"])
    {
        NSLog(@"success");
        
        NSString *scoreResponse = [NSString stringWithFormat:@"คะแนนที่ทำได้ %@ / %lu", [result objectForKey:@"score"], (unsigned long)questionArray.count];
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        alert.backgroundViewColor = [UIColor whiteColor];
        
        [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:20.0f];
        [alert setBodyTextFontFamily:@"DBSaiKrokX-Bold" withSize:40.0f];
        [alert setButtonsTextFontFamily:@"DBSaiKrokX" withSize:20.0f];
        
        [alert addButton:@"PERFORMANCE"
                  target:self
                selector:@selector(performance)];
        
        [alert showCustom:self
                    image:[UIImage imageNamed:@""]
                    color:Color
                    title:titleTestExe
                 subTitle:scoreResponse
         closeButtonTitle:nil
                 duration:0.0f];

    } else {
        
        NSLog(@"unsuccess");
    }
}

- (IBAction)back:(id)sender
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    alert.backgroundViewColor = [UIColor whiteColor];
    
    [alert setTitleFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
    [alert setBodyTextFontFamily:@"DBSaiKrokX-Bold" withSize:30.0f];
    [alert setButtonsTextFontFamily:@"DBSaiKrokX" withSize:20.0f];
    
    [alert addButton:@"ใช่"
              target:self
            selector:@selector(yes)];
    
    [alert showCustom:self
                image:[UIImage imageNamed:@"app_01"]
                color:Color
                title:@"พักการทำข้อสอบนี้?"
             subTitle:@""
     closeButtonTitle:@"ไม่ใช่"
             duration:0.0f];
}

- (void)yes
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)performance
{
//    ProfileViewController *profileView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
//    [self.navigationController pushViewController:profileView animated:YES];
}

- (void)ansTapped:(UIButton*)sender
{
    NSLog(@"ansTapped");
}

@end
