//
//  TestLessonViewController.h
//  Nestle School
//
//  Created by MOREMO on 10/20/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestLessonViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSDictionary *quizzesDic;
    
    NSArray *quizzesArray;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *testlbl;

@property (nonatomic, strong) NSString *cellName;
@property (nonatomic, strong) NSString *cellTid;

@property (nonatomic, strong) NSString *tidTestLesson;

- (IBAction)back:(id)sender;

@end
