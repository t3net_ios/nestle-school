//
//  TestLessonViewController.m
//  Nestle School
//
//  Created by MOREMO on 10/20/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "TestLessonViewController.h"
#import "RESTAPI.h"

#import "TestExerciseViewController.h"

@interface TestLessonViewController ()
@end

@implementation TestLessonViewController

@synthesize testlbl;

@synthesize cellName, cellTid;

@synthesize tidTestLesson;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    testlbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    testlbl.layer.shadowOffset = CGSizeMake(0, 1);
    testlbl.layer.shadowOpacity = 1.0;
    testlbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    testlbl.text = cellName;

    [self getQuizzes];
}

- (void)getQuizzes
{
    NSString *urlString = [NSString stringWithFormat:@"service/quizzes/75"]; //, cellTid];
    NSLog(@"%@",urlString);
    
    quizzesArray = [RESTAPI syncGET:urlString];
    //NSLog(@"%@", [subjectArray description]);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return quizzesArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"BG4"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"BG4"];
    } else {
        background = [UIImage imageNamed:@"BG4"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"testlessonCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    
    quizzesDic = [quizzesArray objectAtIndex:indexPath.row];

    UILabel *name_tag = (UILabel *)[cell viewWithTag:100];
    UILabel *title_tag = (UILabel *)[cell viewWithTag:101];
    UILabel *number_tag = (UILabel *)[cell viewWithTag:102];
    UILabel *made_tag = (UILabel *)[cell viewWithTag:103];
    UILabel *time_tag = (UILabel *)[cell viewWithTag:104];
    
    name_tag.text = cellName;
    //[nametxt sizeToFit];
    
    title_tag.text = [quizzesDic objectForKey:@"title"];
    //[titletxt sizeToFit];
    
    number_tag.text = [NSString stringWithFormat:@"จำนวน  %@  ข้อ",[quizzesDic objectForKey:@"field_rule_number"]];
    made_tag.text = [NSString stringWithFormat:@"มีผู้ทำแล้ว  %@  ",[quizzesDic objectForKey:@"field_person_made"]];
    time_tag.text = [NSString stringWithFormat:@"เวลา  %@  นาที",[quizzesDic objectForKey:@"field_time_made"]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TestExerciseViewController *testExerciseView = [self.storyboard instantiateViewControllerWithIdentifier:@"TestExerciseViewController"];
    quizzesDic = [quizzesArray objectAtIndex:indexPath.row];
    
    testExerciseView.nidTestExe = [quizzesDic objectForKey:@"nid"];
    testExerciseView.titleTestExe = [quizzesDic objectForKey:@"title"];
    testExerciseView.timeTestExe = [quizzesDic objectForKey:@"field_time_made"];
    testExerciseView.numberTestExe = [quizzesDic objectForKey:@"field_rule_number"];

    [self.navigationController pushViewController:testExerciseView animated:YES];
}



- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
