//
//  TestSubjectViewController.h
//  Nestle School
//
//  Created by MOREMO on 10/17/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestSubjectViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSDictionary *subjectDic;
    
    NSArray *subjectArray;
    
    NSString *name;
    NSString *tid;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *testQuizzeslbl;
@property (weak, nonatomic) IBOutlet UILabel *class_idTS;

@property (nonatomic, strong) NSString *cellName;
@property (nonatomic, strong) NSString *cellTid;

- (IBAction)back:(id)sender;

@end
