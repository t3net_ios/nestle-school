//
//  TestSubjectViewController.m
//  Nestle School
//
//  Created by MOREMO on 10/17/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "TestSubjectViewController.h"
#import "RESTAPI.h"

#import "TestLessonViewController.h"

@interface TestSubjectViewController ()
@end

@implementation TestSubjectViewController

@synthesize testQuizzeslbl;
@synthesize class_idTS;
@synthesize cellName, cellTid;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    testQuizzeslbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    testQuizzeslbl.layer.shadowOffset = CGSizeMake(0, 1);
    testQuizzeslbl.layer.shadowOpacity = 1.0;
    testQuizzeslbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    class_idTS.text = cellName;

    [self getSubjcet];
}

- (void)getSubjcet
{
    NSString *urlString = [NSString stringWithFormat:@"service/type_quizzes"]; ///%@", cellTid];
    NSLog(@"%@",urlString);
    
    subjectArray = [RESTAPI syncGET:urlString];
    //NSLog(@"%@", [subjectArray description]);
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return subjectArray.count;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"app_23"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"app_23"];
    } else {
        background = [UIImage imageNamed:@"app_23"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"testsubjectCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    
    subjectDic = [subjectArray objectAtIndex:indexPath.row];
    
    UILabel *name_tag = (UILabel *)[cell viewWithTag:101];
    UILabel *tid_tag = (UILabel *)[cell viewWithTag:102];
    
    name_tag.text = [subjectDic objectForKey:@"name"];
    tid_tag.text = [subjectDic objectForKey:@"tid"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TestLessonViewController *testLessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"TestLessonViewController"];
    
    subjectDic = [subjectArray objectAtIndex:indexPath.row];

    testLessonView.cellName = [subjectDic objectForKey:@"name"];
    testLessonView.cellTid = [subjectDic objectForKey:@"tid"];

    [self.navigationController pushViewController:testLessonView animated:YES];
}



- (IBAction)back:(id)sender
{  
    [self.navigationController popViewControllerAnimated:NO];
}

@end
