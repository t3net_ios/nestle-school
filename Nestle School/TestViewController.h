//
//  TestQuizzesViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *typeQuizzArray;
    NSMutableArray *subjectArray;
    NSMutableArray *displayRows;
    
//    NSString *name;
//    NSString *tid;
//    
//    NSArray *buttonArray;
//    NSArray *tidArray;
//    
//    NSMutableArray *arrayForBool;
//    NSArray *sectionTitleArray;
}
@property (weak, nonatomic) IBOutlet UITableView *testTableView;

@property (weak, nonatomic) IBOutlet UILabel *testQuizzeslbl;

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;

@property (weak, nonatomic) IBOutlet UILabel *name1;
@property (weak, nonatomic) IBOutlet UILabel *name2;

@property (weak, nonatomic) IBOutlet UILabel *tid1;
@property (weak, nonatomic) IBOutlet UILabel *tid2;

@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, strong) NSString *buttonTid;

@end
