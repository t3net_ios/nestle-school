//
//  TestQuizzesViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "TestViewController.h"
#import "RESTAPI.h"

#import "TestLessonViewController.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface TestViewController ()
@end

@implementation TestViewController

@synthesize testQuizzeslbl;

@synthesize button1, button2;
@synthesize name1, name2;
@synthesize tid1, tid2;

@synthesize buttonTitle, buttonTid;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    testQuizzeslbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    testQuizzeslbl.layer.shadowOffset = CGSizeMake(0, 1);
    testQuizzeslbl.layer.shadowOpacity = 1.0;
    testQuizzeslbl.layer.shadowRadius = 1.0;
    
    
    //////////////////////////////////////////////////
    [self getTypeQuizz];
}

- (void)getTypeQuizz
{
    NSString *urlString = [NSString stringWithFormat:@"service/type_quizzes"];
    //NSLog(@"%@",urlString);
    
    typeQuizzArray = [RESTAPI syncGET:urlString];
    subjectArray = [NSMutableArray new];
    displayRows = [NSMutableArray new];

    for (NSDictionary *testDic in typeQuizzArray)
    {
        NSString *t_type = [testDic objectForKey:@"type"];
        NSData *data = [t_type dataUsingEncoding:NSUTF8StringEncoding];
        
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

        NSArray *typeArray = (NSArray*)json;
        [subjectArray addObject:typeArray];
        
        [displayRows addObject:@(0)];
    }
//    NSLog(@"%@",subjectArray);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return typeQuizzArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[displayRows objectAtIndex:section] integerValue];
}

-(NSArray*) indexPathsForSection:(NSInteger)section withNumberOfRows:(NSInteger)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (NSInteger i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}

-(void)sectionButtonTouchUpInside:(UIButton*)sender
{
    NSInteger section = sender.tag;
    
    [sender setSelected:!sender.selected];
    
    [self.testTableView beginUpdates];
    
    BOOL shouldCollapse = !sender.selected;
    if (shouldCollapse) {
        [sender setBackgroundImage:[UIImage imageNamed:@"app_19 copy"] forState:UIControlStateNormal];
        
        NSInteger numOfRows = [self.testTableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [self.testTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [displayRows replaceObjectAtIndex:section withObject:@(0)];
    }
    else {
        [sender setBackgroundImage:[UIImage imageNamed:@"app_22"] forState:UIControlStateNormal];
        
        NSInteger numOfRows = [subjectArray[section] count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [self.testTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [displayRows replaceObjectAtIndex:section withObject:@(numOfRows)];
    }
    [self.testTableView endUpdates];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 67;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67+10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
    [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    result.backgroundColor = [UIColor clearColor];
    result.adjustsImageWhenHighlighted = NO;
    [result setBackgroundImage:[UIImage imageNamed:@"app_19 copy"] forState:UIControlStateNormal];
    
    NSString *title = [[typeQuizzArray objectAtIndex:section] valueForKey:@"name"];
    [result setTitle:title forState:UIControlStateNormal];
    
    [result.titleLabel setFont:[UIFont fontWithName:@"DBSaiKrokX" size:40.0f]];
    [result setTitleColor:Color forState:UIControlStateNormal];
    result.tag = section;
    
    return result;
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self testTableView] numberOfRowsInSection:indexPath.section];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"app_24"];
    } else if (rowIndex == rowCount - 1) {
        //Last Cell
        background = [UIImage imageNamed:@"app_24"];
    } else {
        background = [UIImage imageNamed:@"app_23"];
    }
    
    return background;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"testCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    NSArray *subjects = [subjectArray objectAtIndex:indexPath.section];
    NSDictionary *subject = [subjects objectAtIndex:indexPath.row];
    
    UILabel *name_tag = (UILabel *)[cell viewWithTag:101];
    UILabel *tid_tag = (UILabel *)[cell viewWithTag:102];
    
    name_tag.text = [subject objectForKey:@"name"];
    [name_tag setTextColor:Color];
    tid_tag.text = [subject objectForKey:@"tid"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TestLessonViewController *testLessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"TestLessonViewController"];
    
    NSArray *subjects = [subjectArray objectAtIndex:indexPath.section];
    NSDictionary *subject = [subjects objectAtIndex:indexPath.row];
    
    testLessonView.cellName = [subject objectForKey:@"name"];
    testLessonView.cellTid = [subject objectForKey:@"tid"];
    
    [self.navigationController pushViewController:testLessonView animated:YES];
}



@end
