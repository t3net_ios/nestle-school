//
//  ToolsViewController.h
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToolsViewController : UIViewController
{
    NSString *name;
    NSString *tid;
    
    NSArray *buttonArray;
    NSArray *tidArray;
    
    UIButton *currentButton;
    UILabel *currentTidClass;
    
    NSArray *nameClassArray;
    NSArray *tidClassArray;
}

@property (weak, nonatomic) IBOutlet UILabel *toolslbl;

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;

@property (weak, nonatomic) IBOutlet UILabel *tid1;
@property (weak, nonatomic) IBOutlet UILabel *tid2;

@property (nonatomic, strong) NSString *buttonTid;


@end
