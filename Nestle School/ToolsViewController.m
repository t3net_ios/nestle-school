//
//  ToolsViewController.m
//  Nestle School
//
//  Created by MOREMO on 9/28/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ToolsViewController.h"

#import "RESTAPI.h"

#import "FormulaViewController.h"
#import "ConverterViewController.h"

@interface ToolsViewController ()
@end

@implementation ToolsViewController

@synthesize toolslbl;

@synthesize button1, button2;
@synthesize tid1, tid2;

@synthesize buttonTid;

- (void)viewDidLoad
{
    [super viewDidLoad];

    toolslbl.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    toolslbl.layer.shadowOffset = CGSizeMake(0, 1);
    toolslbl.layer.shadowOpacity = 1.0;
    toolslbl.layer.shadowRadius = 1.0;
    
    NSArray *result = [RESTAPI syncGET:@"service/help-type"];
    
    nameClassArray = [result valueForKeyPath:@"name"];
    tidClassArray = [result valueForKeyPath:@"tid"];
    
    
    buttonArray = [NSArray arrayWithObjects:button1, button2, nil];
    tidArray = [NSArray arrayWithObjects:tid1, tid2, nil];
    
    for (int i = 0; i < tidClassArray.count; i++)
    {
        currentButton = [buttonArray objectAtIndex:i];
        currentTidClass = [tidArray objectAtIndex:i];
        
        NSString *namebutton = [nameClassArray objectAtIndex:i];
        NSString *tidbutton = [tidClassArray objectAtIndex:i];
        
        [currentButton setTitle:namebutton forState:UIControlStateNormal];
        currentTidClass.text = tidbutton;
    }

}

- (IBAction)chooseTool:(id)sender
{
    if ([sender tag] == 101)
    {
        buttonTid = self.tid1.text;
    
        FormulaViewController *formulaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FormulaViewController"];
        
        formulaViewController.tidFormula = self.buttonTid;
        
        [self.navigationController pushViewController:formulaViewController animated:YES];

    }
    
    else if ([sender tag] == 102)
    {
        buttonTid = self.tid2.text;
        
        ConverterViewController *converterViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ConverterViewController"];
        
        converterViewController.tidConverter = self.buttonTid;
        
        [self.navigationController pushViewController:converterViewController animated:YES];
    }
}

@end
