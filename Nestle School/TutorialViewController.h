//
//  TutorialViewController.h
//  Nestle School
//
//  Created by MMOREMO on 11/29/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController <UIScrollViewDelegate>
{
    NSMutableArray *myTutorial;
}

@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageTutorialControl;

@property (weak, nonatomic) IBOutlet UIButton *skipedButton;
- (IBAction)skipButton:(id)sender;

@end
