//
//  TutorialViewController.m
//  Nestle School
//
//  Created by MMOREMO on 11/29/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "TutorialViewController.h"

#define Color [UIColor colorWithRed:70.0/255.0 green:185.0/255.0 blue:230.0/255.0 alpha:1.0]

@interface TutorialViewController ()
@end

@implementation TutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.myScrollView.delegate = self;
    self.pageTutorialControl.pageIndicatorTintColor = [UIColor whiteColor];
    self.pageTutorialControl.currentPageIndicatorTintColor = Color;
    
    myTutorial = [[NSMutableArray alloc] init];
    
    [myTutorial addObject:@"1-copy.png"];
    [myTutorial addObject:@"2-copy.png"];
    [myTutorial addObject:@"3-copy.png"];
    [myTutorial addObject:@"4-copy.png"];
    [myTutorial addObject:@"5-copy.png"];
    
    for (int i = 0; i < [myTutorial count]; i++)
    {
        NSString *imageString = [myTutorial objectAtIndex:i];
        
        UIImage * image = [UIImage imageNamed:imageString];
        
        UIImageView *tutorialImage = [[UIImageView alloc] initWithFrame:CGRectMake(i*self.myScrollView.frame.size.width,
                                                                                   0,self.myScrollView.frame.size.width,
                                                                                   self.myScrollView.frame.size.height)];
        
        [tutorialImage setImage:image];
        [self.myScrollView addSubview:tutorialImage];
    }
    
    self.myScrollView.contentSize = CGSizeMake(self.myScrollView.frame.size.width * [myTutorial count],
                                               self.myScrollView.frame.size.height);
    
    self.pageTutorialControl.currentPage = 0;
    self.pageTutorialControl.numberOfPages = [myTutorial count];
    self.myScrollView.pagingEnabled = YES;
}

-(void)alertComplete
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *tabcontroller = [mainStoryboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    
    tabcontroller.selectedIndex = 2;
    
    UITabBar *tabBar = tabcontroller.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    
    tabBarItem1.selectedImage = [[UIImage imageNamed:@"ico_1_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem1.image = [[UIImage imageNamed:@"ico_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem2.selectedImage = [[UIImage imageNamed:@"ico_2_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem2.image = [[UIImage imageNamed:@"ico_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem3.selectedImage = [[UIImage imageNamed:@"BUTTON3-2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem3.image = [[UIImage imageNamed:@"BUTTON3.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem4.selectedImage = [[UIImage imageNamed:@"ico_3_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem4.image = [[UIImage imageNamed:@"ico_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem5.selectedImage = [[UIImage imageNamed:@"ico_4_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem5.image = [[UIImage imageNamed:@"ico_4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self presentViewController:tabcontroller animated:YES completion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)localScrollView
{
    self.pageTutorialControl.currentPage = localScrollView.contentOffset.x/self.view.frame.size.width;
    
    if (self.pageTutorialControl.currentPage == 4)
    {
        [_skipedButton setImage:[UIImage imageNamed:@"DONE.png"] forState:UIControlStateNormal];
    }
    
}

- (IBAction)skipButton:(id)sender
{
    [self alertComplete];
}



@end
